import React from "react";
import Image from "next/image";

const HeroSection = () => {
  return (
    <>
      <div className="w-[650px] ml-12 mt-24 max-mobile:w-72 max-mobile:ml-16 max-mobile:mt-14 max-laptop1:mt-48 max-laptop1:ml-24 max-laptop1:w-[600px]">
        <Image src="/icons/hero.png" width="1000" height="1000" alt="******" className="max-laptop1:w-96" />
      </div>
    </>
  );
};

export default HeroSection;
