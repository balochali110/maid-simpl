import React, { useState } from "react";
import Select from "react-select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const DateTime = (props) => {
  const timeOption = [
    { value: "09:00 pm", label: "09:00 pm" },
    { value: "10:00 pm", label: "10:00 pm" },
    { value: "11:00 pm", label: "11:00 pm" },
  ];

  const handleOneTimeClick = () => {
    props.setOneTime(!props.onetime);
    props.setWeekly(false);
    props.setBiWeekly(false);
    props.setMonthly(false);
  };

  const handleWeeklyClick = () => {
    props.setOneTime(false);
    props.setWeekly(!props.weekly);
    props.setBiWeekly(false);
    props.setMonthly(false);
  };

  const handleBiWeeklyClick = () => {
    props.setOneTime(false);
    props.setWeekly(false);
    props.setBiWeekly(!props.biWeekly);
    props.setMonthly(false);
  };

  const handleMonthlyClick = () => {
    props.setOneTime(false);
    props.setWeekly(false);
    props.setBiWeekly(false);
    props.setMonthly(!props.monthly);
  };

  return (
    <div>
      <p className="mt-6 font-medium text-lg max-mobile:text-center max-mobile:ml-2">
        When would you like us to come?
      </p>
      <div className="flex max-mobile:block">
        <div className="mt-4 w-1/2 mr-4 max-mobile:w-full max-mobile:ml-3">
          <DatePicker
            selected={props.date}
            onChange={(date) => props.setDate(date)}
            className="border border-2 border-[#8cd790] rounded-md drop-shadow-lg h-[39px] w-full pl-4 outline-none"
            // className="w-full rounded-lg text-center border border-2 border-[#8cd790] "
            placeholderText="Select Date"
          />
        </div>
        <div className="w-1/2 mr-12 mt-4 max-mobile:w-full max-mobile:ml-3">
          <Select
            defaultValue={props.time}
            onChange={props.setTime}
            options={timeOption}
            className="border border-1 border-[#8cd790] rounded-md drop-shadow-lg relative z-10 outline-none"
            placeholder="Select Time"
          />
        </div>
      </div>
      <p className="mt-6 font-semibold text-lg max-mobile:text-center">How often?</p>
      <div className="mt-5 mr-12 max-mobile:mr-0 max-mobile:ml-4">
        <div className="w-full flex max-mobile:block">
          <div
            className={
              props.onetime
                ? "w-1/4 max-mobile:w-full mr-2 bg-[#8cd790] font-semibold text-white px-1 py-1 rounded-full border border-2 drop-shadow-lg border-2 border-[#8cd790]"
                : "w-1/4 max-mobile:w-full mr-2 bg-white px-1 py-1 rounded-full border border-2 border-[#8cd790] drop-shadow-lg"
            }
            onClick={handleOneTimeClick}
          >
            <p className="uppercase text-center tracking-widest cursor-pointer">
              One time
            </p>
          </div>
          <div
            className={
              props.weekly
                ? "w-1/4 max-mobile:w-full max-mobile:mt-2 mr-2 bg-[#8cd790] font-semibold text-white px-1 py-1 rounded-full border border-2 drop-shadow-lg border-2 border-[#8cd790]"
                : "w-1/4 max-mobile:w-full max-mobile:mt-2 mr-2 bg-white px-1 py-1 rounded-full border border-2 border-[#8cd790] drop-shadow-lg"
            }
            onClick={handleWeeklyClick}
          >
            <p className="uppercase text-center tracking-widest cursor-pointer">
              Weekly
            </p>
          </div>
          <div
            className={
              props.biWeekly
                ? "w-1/4 max-mobile:w-full max-mobile:mt-2 mr-2 bg-[#8cd790] font-semibold text-white px-1 py-1 rounded-full border border-2 drop-shadow-lg border-2 border-[#8cd790]"
                : "w-1/4 max-mobile:w-full max-mobile:mt-2 mr-2 bg-white px-1 py-1 rounded-full border border-2 border-[#8cd790] drop-shadow-lg"
            }
            onClick={handleBiWeeklyClick}
          >
            <p className="uppercase text-center tracking-widest cursor-pointer">
              Bi Weekly
            </p>
          </div>
          <div
            className={
              props.monthly
                ? "w-1/4 max-mobile:w-full max-mobile:mt-2 mr-2 bg-[#8cd790] font-semibold text-white px-1 py-1 rounded-full border border-2 drop-shadow-lg border-2 border-[#8cd790]"
                : "w-1/4 max-mobile:w-full max-mobile:mt-2 mr-2 bg-white px-1 py-1 rounded-full border border-2 border-[#8cd790] drop-shadow-lg"
            }
            onClick={handleMonthlyClick}
          >
            <p className="uppercase text-center tracking-widest cursor-pointer">
              Monthly
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DateTime;
