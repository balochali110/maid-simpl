import React from "react";
import { motion } from "framer-motion";
import { useRouter } from "next/router";

const Discount = ({ comments, discount, setDiscount, setComments }) => {
  const router = useRouter();
  const token = router.query.token;
  return (
    <>
      <div className="-ml-2">
        <div className="flex">
          <div className="w-full mr-24 max-mobile:mr-2">
            <div className="bg-transparent px-2 py-4 rounded-lg">
              <p className="font-medium text-lg text-black max-mobile:text-center">
                Comments And Special Instructions
              </p>
              <textarea
                rows={8}
                className="w-full border border-2 border-[#8cd790] rounded-lg mt-5 px-4 py-4 outline-none"
                placeholder="Enter Your Comments Here"
                value={comments}
                onChange={(e) => setComments(e.target.value)}
              />
            </div>
          </div>
        </div>
        <div className="w-full mr-24 max-mobile:mr-4">
          <div className="bg-transparent px-2 py-4 rounded-lg">
            <p className="text-black font-medium text-lg max-mobile:text-center max-mobile:-ml-4">Discount</p>
            <div>
              <input
                type="text"
                className="h-12 rounded-lg border border-2 border-[#8cd790] mt-4 w-3/4 pl-4 pr-4 outline-none max-mobile:w-full"
                placeholder="Enter Discount Code"
                value={discount}
                onChange={(e) => setDiscount(e.target.value)}
              />
              <motion.button
                className="bg-black text-white ml-4 px-12 py-3 rounded-lg font-medium hover:bg-gray-500 max-mobile:w-full max-mobile:ml-0 max-mobile:mt-2"
                whileTap={{ scale: 0.5 }}
              >
                Apply
              </motion.button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Discount;
