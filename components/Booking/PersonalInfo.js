import React from "react";

const PersonalInfo = ({
  firstName,
  setFirstName,
  lastName,
  setLastName,
  email,
  setEmail,
  contact,
  setContact,
  country,
  setCountry,
  state,
  setState,
  zipcode,
  setZipcode,
}) => {
  return (
    <>
      <div className="mt-8 mb-12 rounded-lg mr-24 pt-5 pb-5 max-mobile:mr-4">
        <p className="text-lg font-medium max-mobile:text-center">Personal Information</p>
        <div className="mt-5 flex w-full max-mobile:block">
          <div className="w-1/2 max-mobile:w-full">
            <p className="text-base text-gray-500 font-medium">First Name</p>
            <input
              type="text"
              className="h-10 bg-transparent border border-2 border-[#8cd790] mt-2 w-full rounded-lg pl-4 pr-4 outline-none"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </div>
          <div className="ml-2 w-1/2 max-mobile:w-full max-mobile:mt-3 max-mobile:ml-0">
            <p className="text-base text-gray-500 font-medium">Last Name</p>
            <input
              type="text"
              className="h-10 bg-transparent border border-2 border-[#8cd790] mt-2 w-full rounded-lg pl-4 pr-4 outline-none"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </div>
        </div>
        <div className="mt-5 flex max-mobile:block">
          <div className="w-1/2 max-mobile:w-full">
            <p className="text-base text-gray-500 font-medium">Email Address</p>
            <input
              type="text"
              className="h-10 bg-transparent border border-2 border-[#8cd790] mt-2 w-full rounded-lg pl-4 pr-4 outline-none"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="ml-2 w-1/2 max-mobile:w-full max-mobile:ml-0 max-mobile:mt-3">
            <p className="text-base text-gray-500 font-medium">Phone</p>
            <input
              type="text"
              className="h-10 bg-transparent border border-2 border-[#8cd790] mt-2 w-full rounded-lg pl-4 pr-4 outline-none"
              value={contact}
              onChange={(e) => setContact(e.target.value)}
            />
          </div>
        </div>
        <div className="mt-5 flex max-mobile:block">
          <div className="w-1/2 max-mobile:w-full">
            <p className="text-base text-gray-500 font-medium">Country</p>
            <input
              type="text"
              className="h-10 bg-transparent border border-2 border-[#8cd790] mt-2 w-full rounded-lg pl-4 pr-4 outline-none"
              value={country}
              onChange={(e) => setCountry(e.target.value)}
            />
          </div>
          <div className="ml-2 w-1/2 max-mobile:ml-0 max-mobile:w-full max-mobile:mt-3">
            <p className="text-base text-gray-500 font-medium">City/State</p>
            <input
              type="text"
              className="h-10 bg-transparent border border-2 border-[#8cd790] mt-2 w-full rounded-lg pl-4 pr-4 outline-none"
              value={state}
              onChange={(e) => setState(e.target.value)}
            />
          </div>
        </div>
        <div className="mt-5 flex mr-2 max-mobile:block max-mobile:mr-0">
          <div className="w-1/2 max-mobile:w-full">
            <p className="text-base text-gray-500 font-medium">Postal Code</p>
            <input
              type="text"
              className="h-10 bg-transparent border border-2 border-[#8cd790] mt-2 w-full rounded-lg pl-4 pr-4 outline-none"
              value={zipcode}
              onChange={(e) => setZipcode(e.target.value)}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default PersonalInfo;
