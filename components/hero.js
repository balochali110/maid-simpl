import { motion } from "framer-motion";
import HeroSection from "./herosection";
import { useRouter } from "next/router";

const Hero = () => {
  const router = useRouter();

  const booknow = () => {
    router.push("/bookus");
  };
  return (
    <>
      <div
        className="flex pt-10 pb-36 max-mobile:pb-20"
        style={{
          fontFamily: "roboto",
        }}
      >
        <div
          className={`
            w-1/2 ml-56 mt-64 
            max-mobile:ml-4 max-mobile:mr-4 max-mobile:mt-8 max-mobile:w-full max-mobile:text-center

          `}
        >
          <motion.div
            className="mb-8"
            initial={{ x: -1000 }}
            animate={{ x: 0 }}
            transition={{ duration: 4, type: "spring", damping: 10 }}
          >
            <div
              className={`
                text-7xl font-bold leading-snug tracking-tight text-black 
                max-mobile:text-3xl max-laptop1:text-5xl
              `}
            >
              LOREM IPSUM DOLOR <br />
              <span className="pb-[-18px] relative z-20 max-mobile:-z-40">
                IPSUM DOLOR
              </span>
              <div className="border-b-8 border-[#8CD790] w-[445px] relative z-10 -mt-8 ml-1 max-mobile:hidden max-laptop1:hidden" />
            </div>
              <div className="w-1/2 -mt-5 min-[450px]:hidden">
                <HeroSection />
              </div>
            <p
              className={`py-5 text-xl leading-normal mt-4 font-normal text-[#4D4D4D] lg:text-xl xl:text-2xl dark:text-gray-300 mr-36
                max-mobile:mr-0 max-mobile:text-base max-laptop1:text-lg max-laptop1:mr-4`}
            >
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Lorem
              ipsum dolor sit amet consectetur adipisicing elit.
            </p>
            <div className="max-mobile:flex max-mobile:place-content-center">
              <motion.button
                className={`
                  flex items-start w-48 place-content-center px-8 py-4 mt-4 text-lg text-center text-white font-bold bg-[#4A4A48] rounded-lg 
                  max-mobile:py-1 max-mobile:text-sm 
                `}
                whileTap={{ scale: 0.3 }}
                onClick={booknow}
              >
                Book Now
              </motion.button>
            </div>
          </motion.div>
        </div>
        <div className="w-1/2 max-mobile:hidden">
          <HeroSection />
        </div>
      </div>
    </>
  );
};

export default Hero;
