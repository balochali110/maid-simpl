import React from "react";
import heroImg from "../../public/img/abstract.png";
import Image from "next/image";
import { motion } from "framer-motion";

const Card = (props) => {
  return (
    <>
      <div className="pl-16">
        <motion.div
          className="bg-[#285943] w-80 h-full pb-4 rounded-lg"
          whileHover={{ scale: 1.2 }}
        >
          <span className="flex place-content-center pt-12">
            <Image
              src={heroImg}
              width="126"
              height="127"
              className={"bg-white rounded-full px-4 py-4"}
              alt="Hero Illustration"
              loading="eager"
              placeholder="blur"
            />
          </span>
          <div className="text-[#fcfbf7] pt-8">
            <p className="flex place-content-center font-bold text-2xl pb-3">
              {props.pretitle}
            </p>
            <p className="flex place-content-center pl-6 pr-6 text-center">
              {props.desc}
            </p>
          </div>
        </motion.div>
      </div>
    </>
  );
};

export default Card;
