import React, { useState } from "react";
import Image from "next/image";
import { motion } from "framer-motion";
import { useRouter } from "next/router";

const ServiceProviderCard = (props) => {
  const router = useRouter();
  const [toggle, setToggle] = useState(false);
  const handleViewDetails = () => {
    router.push("/viewdetails");
  };
  return (
    <>
      <motion.div
        className={
          toggle || props.isSelected
            ? "mt-8 bg-[#8cd790] text-white border border-2 mr-16 max-mobile:mr-6 rounded-lg pl-5 pt-5 pb-5 border-[#8cd790] drop-shadow-2xl cursor-pointer"
            : "mt-8 border border-2 mr-16 max-mobile:mr-6 rounded-lg pl-5 pt-5 pb-5 border-[#8cd790] drop-shadow-2xl cursor-pointer"
        }
        whileHover={{ scale: 1.2 }}
        onMouseEnter={() => setToggle(true)}
        onMouseLeave={() => setToggle(false)}
        onClick={() => props.onSelect(props.card.id)}
        style={{
          borderRadius: "50px",
          fontFamily: "roboto"
        }}
      >
        <div className="flex">
          <Image
            src="/icons/avatar.png"
            width="110"
            height="110"
            alt=""
            className="bg-white rounded-full border border-2 border-[#8cd790]"
          />
          <div className="mt-7 ml-7">
            <p
              className="font-semibold text-2xl"
              style={{ fontFamily: "roboto" }}
            >
              Micheal
            </p>
            <p className="text-lg font-meidum">Location</p>
          </div>
          <div>
            <div className="flex mt-6 bg-[#E8F7E9] ml-32 rounded-full px-3 py-1">
              <span>
                <Image
                  src="/icons/greenstar.png"
                  width="25"
                  height={25}
                  alt=""
                />
              </span>
              <p className="ml-2 font-semibold text-black">Top Rated</p>
            </div>
          </div>
        </div>
        <div className="ml-7 pt-5">
          <p className="font-semibold text-xl">About</p>
          <p className="mt-1 mr-5">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero
            dolorum inventore eum voluptate.
          </p>
        </div>
        <div className="flex place-content-end mr-8 mt-2">
          <motion.button
            className="bg-black text-white px-5 py-2 rounded-lg font-medium"
            whileTap={{ scale: 0.5 }}
            onClick={handleViewDetails}
          >
            View Profile
          </motion.button>
        </div>
      </motion.div>
    </>
  );
};

export default ServiceProviderCard;

// <motion.div
// className={
//   toggle || props.isSelected
//     ? "bg-[#8cd790] w-72 pt-8 rounded-lg border border-2 border-[#8cd790] pb-8 cursor-pointer ml-8 mt-16"
//     : "bg-[#ffffff] w-72 pt-8 rounded-lg border border-2 border-[#8cd790] pb-8 cursor-pointer ml-8 mt-16"
// }
// whileHover={{ scale: 1.3 }}
// onMouseEnter={() => setToggle(true)}
// onMouseLeave={() => setToggle(false)}
// >
// <div className="flex w-full place-content-center">
//   <Image
//     src="/icons/avatar.png"
//     width="140"
//     height="140"
//     alt=""
//     className="bg-white rounded-full px-2 py-2 border border-2 border-[#8cd790]"
//   />
// </div>
// <p
//   className={
//     toggle || props.isSelected
//       ? "font-medium text-2xl text-white text-center mt-4"
//       : "font-medium text-2xl text-black text-center mt-4"
//   }
// >
//   {props.name}
// </p>
// <div className="flex place-content-center mt-2">
//   <Image
//     src="/icons/star.png"
//     width="25"
//     height="25"
//     alt=""
//     className="ml-2"
//   />
//   <Image
//     src="/icons/star.png"
//     width="25"
//     height="25"
//     alt=""
//     className="ml-2"
//   />
//   <Image
//     src="/icons/star.png"
//     width="25"
//     height="25"
//     alt=""
//     className="ml-2"
//   />
//   <Image
//     src="/icons/star.png"
//     width="25"
//     height="25"
//     alt=""
//     className="ml-2"
//   />
//   <Image
//     src="/icons/star.png"
//     width="25"
//     height="25"
//     alt=""
//     className="ml-2"
//   />
// </div>
// <span>
//   <p
//     className={
//       toggle || props.isSelected
//         ? "text-center font-medium text-white underline underline-offset-4 mt-2"
//         : "text-center font-medium text-black underline underline-offset-4 mt-2"
//     }
//     onClick={handleViewDetails}
//   >
//     View Details
//   </p>
// </span>
// <div className="flex place-content-center mt-4">
//   <motion.button
//     className={
//       toggle || props.isSelected
//         ? "bg-[#333333] text-white px-10 py-2 rounded-full font-medium text-lg"
//         : "bg-[#8cd790] text-white px-10 py-2 rounded-full font-medium text-lg"
//     }
//     whileTap={{ scale: 0.8 }}
//     onClick={() => props.onSelect(props.card.id)}
//   >
//     {props.selected || props.isSelected ? "Selected" : "Select"}
//   </motion.button>
// </div>
// </motion.div>
