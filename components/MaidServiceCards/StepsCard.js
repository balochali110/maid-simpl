import React from "react";

const StepsCard = (props) => {
  return (
    <div className="flex place-content-center mt-24 max-mobile:mt-6">
      <div className="ml-14 max-mobile:ml-1 max-mobile:flex max-laptop1:ml-4">
        <div className="w-20 h-20 pt-[3px] pl-[3px] border-[#8cd790] rounded-full ml-14 border-2 border-dashed max-mobile:-ml-8">
          <p className="bg-[#4a4a48] w-[70px] h-[70px] pt-3 text-4xl text-[#8cd790] rounded-full border-2 text-center font-normal">
            {props.id}
          </p>
        </div>
        <p className="text-lg text-center w-48 mt-2 font-normal max-mobile:w-40 max-mobile:text-base max-mobile:font-semibold max-mobile:mt-4 max-mobile:text-left max-mobile:ml-5">
          {props.desc}
        </p>
      </div>
    </div>
  );
};

export default StepsCard;
