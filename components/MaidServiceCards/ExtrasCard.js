import React, { useState } from "react";
import Image from "next/image";

const ExtrasCard = (props) => {
  return (
    <>
      <div>
        <p className="text-lg font-medium mt-6 max-mobile:text-center">Add extras</p>
        <div className="w-full mt-4 flex -ml-1 max-mobile:grid max-mobile:grid-cols-2 max-mobile:ml-3 max-mobile:gap-2">
          <div
            className={
              props.ovenToggle
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
            }
            onClick={props.handleOvenClick}
          >
            <Image
              src="/img/oven.png"
              width="100"
              height="70"
              alt=""
              className={props.ovenToggle ? "invert" : ""}
            />
          </div>
          <div
            className={
              props.wallToggle
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9" 
            }
            onClick={props.handleWallToggle}
          >
            <Image
              src="/img/wall.png"
              width="100"
              height="70"
              alt=""
              className={props.wallToggle ? "invert" : ""}
            />
          </div>
          <div
            className={
              props.windowToggle
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
            }
            onClick={props.handleWindowToggle}
          >
            <Image
              src="/img/window.png"
              width="100"
              height="70"
              alt=""
              className={props.windowToggle ? "invert" : ""}
            />
          </div>
          <div
            className={
              props.fridgeToggle
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
            }
            onClick={props.handleFridgeToggle}
          >
            <Image
              src="/img/fridge.png"
              width="100"
              height="70"
              alt=""
              className={props.fridgeToggle ? "invert" : ""}
            />
          </div>
          <div
            className={
              props.cabinetsToggle
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
            }
            onClick={props.handleCabinetsToggle}
          >
            <Image
              src="/img/cabinets.png"
              width="100"
              height="70"
              alt=""
              className={props.cabinetsToggle ? "invert" : ""}
            />
          </div>
          <div
            className={
              props.organizingToggle
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
            }
            onClick={props.handleOrganizingToggle}
          >
            <Image
              src="/img/organizing.png"
              width="100"
              height="70"
              alt=""
              className={props.organizingToggle ? "invert" : ""}
            />
          </div>
        </div>
        <div className="w-full mt-8 flex -ml-1 max-mobile:grid max-mobile:grid-cols-2 max-mobile:ml-3 max-mobile:gap-2 max-mobile:mt-2">
          <div
            className={
              props.dishwasherToggle 
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
            }
            onClick={props.handleDishWasherClick}
          >
            <Image
              src="/img/dishwasher.png"
              width="100"
              height="70"
              alt=""
              className={props.dishwasherToggle ? "invert" : ""}
            />
          </div>
          <div
            className={
              props.garageToggle
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
            }
            onClick={props.handleGarageClick}
          >
            <Image
              src="/img/garage.png"
              width="100"
              height="70"
              alt=""
              className={props.garageToggle ? "invert" : ""}
            />
          </div>
          <div
            className={
              props.microwaveToggle
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
            }
            onClick={props.handleMicrowaveClick}
          >
            <Image
              src="/img/microwave.png"
              width="100"
              height="70"
              alt=""
              className={props.microwaveToggle ? "invert" : ""}
            />
          </div>
          <div
            className={
              props.laundryToggle
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
            }
            onClick={props.handleLaundryClick}
          >
            <Image
              src="/img/laundary.png"
              width="100"
              height="70"
              alt=""
              className={props.laundryToggle ? "invert" : ""}
            />
          </div>
          <div
            className={
              props.blindsToggle
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
            }
            onClick={props.handleBlindsClick}
          >
            <Image
              src="/img/blinds.png"
              width="100"
              height="70"
              alt=""
              className={props.blindsToggle ? "invert" : ""}
            />
          </div>
          <div
            className={
              props.washerToggle
                ? "bg-[#8cd790] w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
                : "bg-white w-32 border drop-shadow px-5 py-5 rounded-lg mr-7 border-2 border-[#8cd790] max-laptop1:w-28 max-laptop1:mr-9"
            }
            onClick={props.handleWasherClick}
          >
            <Image
              src="/img/washer.png"
              width="100"
              height="70"
              alt=""
              className={props.washerToggle ? "invert" : ""}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default ExtrasCard;
