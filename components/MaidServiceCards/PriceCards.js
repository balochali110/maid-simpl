import React, { useState } from "react";
import { motion } from "framer-motion";

const PriceCards = () => {
  const [toggle, setToggle] = useState(false);
  const [hover, setHover] = useState(false);
  return (
    <div
      className="w-96 mr-24 text-black border border-2 border-[#8CD790] rounded-[30px] relative z-10 max-mobile:w-80 max-mobile:ml-10 max-mobile:mb-10 max-laptop1:mr-4"
      onMouseEnter={() => setToggle(true)}
      onMouseLeave={() => setToggle(false)}
      style={{
        fontFamily: "roboto",
      }}
    >
      <motion.div
        className="text-center font-sans rounded-[30px] bg-white pt-12 pb-6 hover:bg-[#8cd790] hover:text-white"
        whileHover={{ scale: 1.1 }}
      >
        <p className="text-4xl tracking-widest font-bold">Regular</p>
        <p className="tracking-widest mt-2 text-xl pb-4">Starting at</p>

        <p
          className={
            toggle
              ? "text-8xl font-bold pb-4"
              : "text-8xl font-bold pb-4 text-[#8cd790]"
          }
        >
          $100
        </p>
        <p className="ml-4 mr-4 mt-7">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non nihil
          sunt labore incidunt enim
        </p>
        <motion.button
          className={
            toggle
              ? "w-1/3 mt-8 h-12 font-bold text-white bg-[#4A4A48] mb-2 rounded-md"
              : "w-1/3 mt-8 h-12 font-bold text-black bg-white border border-2 border-black mb-2 rounded-md"
          }
          whileTap={{ scale: 0.8 }}
        >
          Book Now
        </motion.button>
      </motion.div>
    </div>
  );
};

export default PriceCards;
