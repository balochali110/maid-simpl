import React from "react";
import Image from "next/image";

const TestimonialCard = (props) => {
  return (
    <div className="rounded-lg bg-white drop-shadow ml-12 max-mobile:ml-2 max-mobile:mr-12" style={{fontFamily: "roboto"}}>
      <Image
        src={props.img}
        width="1700"
        height="1700"
        className={"w-72"}
        alt="Hero Illustration"
      />
      <div className="mt-[-20px] ml-5">
        <Image
          src="/icons/icon5.png"
          width="70"
          height="70"
          className={"w-12"}
          alt="Hero Illustration"
        />
      </div>
      <p className="w-72 pl-6 mt-4 pb-4 pr-4">{props.text}</p>
      <p className="pl-6 font-bold font-roboto pb-4 text-lg">{props.name}</p>
    </div>
  );
};

export default TestimonialCard;
