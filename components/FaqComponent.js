import React, { useState } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { useEffect } from "react";

const faqItems = [
  {
    question: "Question 1",
    answer:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi vitae impedit incidunt excepturi deserunt dolorem optio totam repellat harum voluptas soluta laboriosam corrupti omnis tempora, saepe reiciendis aperiam autem neque.",
  },
  {
    question: "Question 2",
    answer:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi vitae impedit incidunt excepturi deserunt dolorem optio totam repellat harum voluptas soluta laboriosam corrupti omnis tempora, saepe reiciendis aperiam autem neque.",
  },
  {
    question: "Question 3",
    answer:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi vitae impedit incidunt excepturi deserunt dolorem optio totam repellat harum voluptas soluta laboriosam corrupti omnis tempora, saepe reiciendis aperiam autem neque.",
  },
  {
    question: "Question 4",
    answer:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi vitae impedit incidunt excepturi deserunt dolorem optio totam repellat harum voluptas soluta laboriosam corrupti omnis tempora, saepe reiciendis aperiam autem neque.",
  },
  {
    question: "Question 5",
    answer:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi vitae impedit incidunt excepturi deserunt dolorem optio totam repellat harum voluptas soluta laboriosam corrupti omnis tempora, saepe reiciendis aperiam autem neque.",
  },
];

const FaqComponent = () => {
  const [activeItems, setActiveItems] = useState([]);

  const handleClick = (index) => {
    setActiveItems((prevActiveItems) =>
      prevActiveItems.includes(index)
        ? prevActiveItems.filter((item) => item !== index)
        : [...prevActiveItems, index]
    );
  };

  const [size, setSize] = useState(80)

  useEffect(() => {
    if(window.screen.width <= 450) {
      setSize(210)
    }
  }, [])

  return (
    <>
      {faqItems.map((item, index) => (
        <motion.div
          key={index}
          className={`${
            activeItems.includes(index)
              ? "bg-[#EEFEF4]"
              : "bg-[#F5F5F5]"
          } w-[1220px] ml-40 mb-6 rounded-lg cursor-pointer max-mobile:w-80 max-mobile:ml-8`}
          onClick={() => handleClick(index)}
          layout
          style={{
            fontFamily: "roboto",
          }}
        >
          <div className="flex">
            <p className="ml-5 text-lg font-semibold text-[#424242] pt-4 pb-4">
              {item.question}
            </p>
            <div className="mt-5 ml-[1070px] max-mobile:ml-44">
              <motion.svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={2.5}
                stroke="currentColor"
                className={`w-6 h-6`}
                initial={false}
                animate={activeItems.includes(index) ? "open" : "closed"}
                variants={{
                  open: { rotate: 180 },
                  closed: { rotate: 0 },
                }}
                transition={{ duration: 0.4 }}
              >
                <motion.path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M19.5 8.25l-7.5 7.5-7.5-7.5"
                  initial={false}
                  animate={activeItems.includes(index) ? "open" : "closed"}
                  variants={{
                    open: { pathLength: 1 },
                    closed: { pathLength: 1 },
                  }}
                  transition={{ duration: 0.3 }}
                />
              </motion.svg>
            </div>
          </div>
          <AnimatePresence>
            {activeItems.includes(index) && (
              <motion.div
                className="ml-5 text-lg text-gray-500 pb-2"
                initial={{ height: 0, opacity: 0 }}
                animate={{ height: size, opacity: 1 }}
                exit={{ height: 0, opacity: 0 }}
                transition={{ duration: 0.3 }}
              >
                {item.answer}
              </motion.div>
            )}
          </AnimatePresence>
        </motion.div>
      ))}
    </>
  );
};

export default FaqComponent;
