import React, { useState } from "react";
import FaqComponent from "./FaqComponent";
import { motion } from "framer-motion";

const Faqs = () => {
  const faqData = [
    {
      question: "What is React?",
      answer: "React is a JavaScript library for building user interfaces.",
    },
    {
      question: "How do I install React?",
      answer:
        "You can install React by using npm or yarn. For example, run `npm install react`.",
    },
  ];

  return (
    <>
      <div className="pt-44 pb-24 font-[#424242] max-mobile:pt-20" id="faq" style={{
        fontFamily: "roboto"
      }}>
        <p className="text-center text-4xl font-semibold mb-36 max-mobile:mb-16">
          <span className="border-b-4 border-[#8cd790] ">FAQs</span>
        </p>
        <motion.div className="ml-36 mr-36 max-mobile:ml-2 max-mobile:mr-0 max-laptop1:-ml-10 max-laptop1:mr-12" layout transition={{ duration: 0.1 }}>
          <FaqComponent data={faqData} />
        </motion.div>
      </div>
    </>
  );
};

export default Faqs;
