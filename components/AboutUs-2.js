import React from "react";
import Image from "next/image";
import MaidCards from "./MaidCards";
const AboutUs2 = () => {
  return (
    <div className="mt-6 pb-36" style={{ fontFamily: "roboto" }}>
      <div className="flex place-content-start ml-36 mt-[-200px] max-laptop1:ml-4 max-mobile:hidden">
        <Image src="/icons/shade2.png" width="380" height="380" alt="" />
      </div>
      <div className="flex relative z-10">
        <div className="flex ml-96 mt-[-210px] max-laptop1:ml-36 max-mobile:mt-12 max-mobile:block max-mobile:ml-0">
          <span className="w-2/6 mt-12 max-mobile:w-full">
            <p className="text-3xl font-semibold mb-5 max-laptop1:text-2xl max-mobile:text-center max-mobile:text-2xl">
              A Maid Service
            </p>
            <p className="text-[#8cd790] font-bold text-7xl pb-2 max-laptop1:text-5xl max-mobile:text-5xl max-mobile:text-center">
              You Can Trust
            </p>
            <p className="mt-5 tracking-widest font-semibold max-mobile:text-center max-mobile:pl-6 max-mobile:pr-6">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat
              commodi, facere, incidunt veniam cupiditate dignissimos autem
              minus unde qui, repellendus atque sint? Ea nihil ipsam officiis
              exercitationem amet sint omnis.
            </p>
          </span>
          <div className="-mt-32 max-mobile:mt-6">
            <div className="flex w-4/6 pl-24 max-mobile:block max-mobile:w-full max-mobile:pl-14 max-mobile:pr-12">
              <span className="">
                <MaidCards />
              </span>
              <span className="ml-8">
                <MaidCards />
              </span>
            </div>
            <div className="flex w-4/6 pl-24 mt-7  max-mobile:block max-mobile:w-full max-mobile:pl-14 max-mobile:pr-12">
              <span className="">
                <MaidCards />
              </span>
              <span className="ml-8">
                <MaidCards />
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="flex place-content-end mr-44 mt-[-500px] max-laptop1:mr-4 max-mobile:hidden">
        <Image src="/icons/icon10.png" width="350" height="250" alt="" />
      </div>
      <div className="flex place-content-start ml-[910px] mt-16 max-mobile:hidden">
        <Image src="/icons/icon9.png" width="150" height="250" alt="" />
      </div>
    </div>
  );
};

export default AboutUs2;
