import React from "react";

const ProfileAddress = () => {
  return (
    <>
      <div className="mt-12 text-xl font-semibold w-full rounded-lg pt-2 mb-12">
        <div className="border border-b-1 pl-8 pt-4 mr-56 border-black rounded-lg">
          <div className="flex w-full mb-2">
            <p className="w-5/6">Address</p>
            <div className="cursor-pointer ml-28">
              <p className="flex rounded-full w-full border pb-1 text-base border-1 border-black px-4 text-black font-semibold pt-1">
                Edit
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-5 h-5 ml-2"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                  />
                </svg>
              </p>
            </div>
          </div>
          {/* Name */}
          <div className="mt-12 flex w-full">
            <span className="w-1/4">
              <p className="text-base text-gray-400">Country</p>
              <p className="text-lg text-black font-semibold">United States</p>
            </span>
            <span className="">
              <p className="text-base text-gray-400">City</p>
              <p className="text-lg text-black font-semibold">Orlando</p>
            </span>
          </div>
          {/* Email */}
          <div className="mt-12 flex w-full mb-6">
            <span className="w-1/4">
              <p className="text-base text-gray-400">Postal Code</p>
              <p className="text-lg text-black font-semibold">
                32789
              </p>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfileAddress;
