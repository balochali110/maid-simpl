import React, { useState } from "react";
import ExtrasCard from "./MaidServiceCards/ExtrasCard";
import DateTime from "./Booking/DateTime";
import PersonalInfo from "./Booking/PersonalInfo";
import Discount from "./Booking/Discount";
import Image from "next/image";
import { useRouter } from "next/router";
import { motion } from "framer-motion";

const BookUs = () => {
  const router = useRouter();

  // states for posting booking
  const [regular, setRegular] = useState(false);
  const [regular1, setRegular1] = useState(false);

  const handleRegular = () => {
    setRegular(!regular);
    setRegular1(false);
  };

  const handleRegular1 = () => {
    setRegular(false);
    setRegular1(!regular1);
  };

  const [bedroom, setBedroom] = useState(0);
  const handleCountBedroomPlus = () => {
    let count = bedroom + 1;
    setBedroom(count);
  };
  const handleCountBedroomMinus = () => {
    let count = bedroom - 1;
    if (count <= 0) {
      setBedroom(0);
    } else {
      setBedroom(count);
    }
  };

  const [drawing, setDrawing] = useState(0);
  const handleCountDrawingPlus = () => {
    let count = drawing + 1;
    setDrawing(count);
  };
  const handleCountDrawingMinus = () => {
    let count = drawing - 1;
    if (count <= 0) {
      setDrawing(0);
    } else {
      setDrawing(count);
    }
  };

  const [kitchen, setKitchen] = useState(0);
  const handleCountKitchenPlus = () => {
    let count = kitchen + 1;
    setKitchen(count);
  };
  const handleCountKitchenMinus = () => {
    let count = kitchen - 1;
    if (count <= 0) {
      setKitchen(0);
    } else {
      setKitchen(count);
    }
  };

  // handle extras
  const [ovenToggle, setOvenToggle] = useState(false);
  const [wallToggle, setWallToggle] = useState(false);
  const [windowToggle, setWindowToggle] = useState(false);
  const [fridgeToggle, setFridgeToggle] = useState(false);
  const [cabinetsToggle, setCabinetsToggle] = useState(false);
  const [organizingToggle, setOrganizingToggle] = useState(false);
  const [dishwasherToggle, setDishwasherToggle] = useState(false);
  const [garageToggle, setGarageToggle] = useState(false);
  const [microwaveToggle, setMicrowaveToggle] = useState(false);
  const [laundryToggle, setLaundryToggle] = useState(false);
  const [blindsToggle, setBlindsToggle] = useState(false);
  const [washerToggle, setWasherToggle] = useState(false);

  const handleOvenClick = () => {
    setOvenToggle(!ovenToggle);
  };

  const handleWallToggle = () => {
    setWallToggle(!wallToggle);
  };

  const handleWindowToggle = () => {
    setWindowToggle(!windowToggle);
  };

  const handleFridgeToggle = () => {
    setFridgeToggle(!fridgeToggle);
  };

  const handleCabinetsToggle = () => {
    setCabinetsToggle(!cabinetsToggle);
  };

  const handleOrganizingToggle = () => {
    setOrganizingToggle(!organizingToggle);
  };

  const handleDishWasherClick = () => {
    setDishwasherToggle(!dishwasherToggle);
  };

  const handleGarageClick = () => {
    setGarageToggle(!garageToggle);
  };

  const handleMicrowaveClick = () => {
    setMicrowaveToggle(!microwaveToggle);
  };

  const handleLaundryClick = () => {
    setLaundryToggle(!laundryToggle);
  };

  const handleBlindsClick = () => {
    setBlindsToggle(!blindsToggle);
  };

  const handleWasherClick = () => {
    setWasherToggle(!washerToggle);
  };

  const [time, setTime] = useState("");
  const [date, setDate] = useState(new Date());

  // how often
  const [onetime, setOneTime] = useState(false);
  const [weekly, setWeekly] = useState(false);
  const [biWeekly, setBiWeekly] = useState(false);
  const [monthly, setMonthly] = useState(false);

  //personal info
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [contact, setContact] = useState("");
  const [country, setCountry] = useState("")
  const [state, setState] = useState("");
  const [zipcode, setZipcode] = useState("");
  const [comments, setComments] = useState("");
  const [discount, setDiscount] = useState("");

  //book us
  const handleBookUs = async () => {
    let extraArr = [
      { name: "oven", toggle: ovenToggle },
      { name: "wall", toggle: wallToggle },
      { name: "window", toggle: windowToggle },
      { name: "fridge", toggle: fridgeToggle },
      { name: "cabinets", toggle: cabinetsToggle },
      { name: "organizing", toggle: organizingToggle },
      { name: "dishwasher", toggle: dishwasherToggle },
      { name: "garage", toggle: garageToggle },
      { name: "microwave", toggle: microwaveToggle },
      { name: "laundry", toggle: laundryToggle },
      { name: "blinds", toggle: blindsToggle },
      { name: "washer", toggle: washerToggle },
    ];
    let extraArrs = [];
    extraArr.map((item) => {
      if (item.toggle == true) {
        extraArrs.push(item.name);
      }
    });

    const dateFUll =
      date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();

    const often = [
      { name: "onetime", toggle: onetime },
      { name: "weekly", toggle: weekly },
      { name: "biWeekly", toggle: biWeekly },
      { name: "monthly", toggle: monthly },
    ];

    let selected;
    often.map((item) => {
      if (item.toggle == true) {
        selected = item.name;
      }
    });

    let rooms = [
      { Bedroom: bedroom },
      { Drawing: drawing },
      { Kitchen: kitchen },
    ];
    
    const len = await fetch("/api/booking");
    const lenResponse = await len.json();

    const data = {
      id: lenResponse.result.length + 1,
      servicetype: regular === true ? "base" : "deep",
      home: rooms,
      extras: extraArrs,
      date: dateFUll,
      time: time.value,
      howoften: selected,
      firstname: firstName,
      lastname: lastName,
      email: email,
      contact: contact,
      country: country,
      state: state,
      zipcode: zipcode,
      comments: comments,
      discount: discount,
    };
    
    let postData = await fetch("/api/booking", {
      method: "POST",
      headers: {
        "Content-Type" : "application/json"
      },
      body: JSON.stringify(data)
    })

    let responseData = await postData.json();
    
    if(responseData.success === true){
      router.push("/serviceprovider")
    } 
  };

  return (
    <>
      <div className="bg-[#EEFEF4] pb-12" style={{ fontFamily: "roboto" }}>
        <p className="text-4xl font-semibold text-center pt-44 pb-12 max-mobile:pt-24 max-mobile:text-2xl">
          Complete Your Booking
        </p>
        <div className="flex place-content-end mr-[330px] max-laptop1:mr-[115px] max-mobile:hidden">
          <Image src="/icons/icon8.png" height="180" width="180" alt="" />
        </div>
        <div className="flex place-content-center -mt-28 max-mobile:ml-8 max-mobile:mr-8 max-mobile:-mt-4">
          <div className="bg-[#FFFFFF] w-[117vh] rounded-lg border max-mobile:w-full">
            <div className="ml-24 mt-16 max-mobile:ml-4">
              <p className="text-lg font-medium max-mobile:text-center">Choose Service Type</p>
              <div className="mr-24 mt-4 flex max-mobile:block max-mobile:mr-4">
                <div className="w-1/2 max-mobile:w-full">
                  <p
                    className={
                      !regular
                        ? "text-center text-xl font-medium border-2 py-2 rounded-full cursor-pointer border-[#8cd790] mr-2 hover:bg-[#8cd790] hover:text-white hover:border-[#8cd790]"
                        : "text-center text-xl font-medium bg-[#8cd790] text-white border-2 border-[#8cd790] py-2 rounded-full cursor-pointer mr-2"
                    }
                    onClick={handleRegular}
                  >
                    Base Clean
                  </p>
                </div>
                <div className="w-1/2 ml-4 max-mobile:w-full max-mobile:ml-0 max-mobile:mt-4">
                  <p
                    className={
                      !regular1
                        ? "text-center text-xl font-medium border-2 py-2 rounded-full cursor-pointer ml-4 max-mobile:ml-0 max-mobile:mr-2 border-[#8cd790] hover:bg-[#8cd790] hover:text-white hover:border-[#8cd790]"
                        : "text-center text-xl font-medium bg-[#8cd790] text-white border-2 border-[#8cd790] py-2 rounded-full cursor-pointer ml-4"
                    }
                    onClick={handleRegular1}
                  >
                    Deep Clean
                  </p>
                </div>
              </div>
              <p className="text-lg font-medium mt-6 max-mobile:text-center">Describe Your Home</p>
              <div className="mr-24 mt-4 max-mobile:mr-8 max-mobile:ml-2">
                <div className="flex w-full max-mobile:block">
                  <div className="flex w-full rounded-lg border border-[#8cd790] border-2">
                    <p className="w-5/6 text-lg font-medium py-2 rounded-lg cursor-pointer mr-2 pl-6">
                      Bedroom
                    </p>
                    <div className="flex w-2/6 mt-[4px] mr-4">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6 cursor-pointer mt-2"
                        onClick={handleCountBedroomMinus}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M19.5 12h-15"
                        />
                      </svg>

                      <input
                        type="text"
                        className="w-3/6 text-center  border-none outline-none rounded-lg"
                        value={bedroom}
                        disabled
                      />
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6 cursor-pointer mt-2"
                        onClick={handleCountBedroomPlus}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M12 4.5v15m7.5-7.5h-15"
                        />
                      </svg>
                    </div>
                  </div>
                  <div className="flex w-full rounded-lg border ml-4 border-[#8cd790] border-2 max-mobile:mr-8 max-mobile:ml-0 max-mobile:mt-3">
                    <p className="w-5/6 text-lg font-medium py-2 rounded-lg cursor-pointer mr-2 pl-6">
                      Drawing Room
                    </p>
                    <div className="flex w-2/6 mt-[4px] mr-4">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6 cursor-pointer mt-2"
                        onClick={handleCountDrawingMinus}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M19.5 12h-15"
                        />
                      </svg>

                      <input
                        type="text"
                        className="w-3/6 text-center  border-none outline-none rounded-lg"
                        value={drawing}
                        disabled
                      />
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6 cursor-pointer mt-2"
                        onClick={handleCountDrawingPlus}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M12 4.5v15m7.5-7.5h-15"
                        />
                      </svg>
                    </div>
                  </div>
                  <div className="flex w-full rounded-lg border ml-4 border-[#8cd790] border-2 max-mobile:mr-8 max-mobile:ml-0 max-mobile:mt-3">
                    <p className="w-5/6 text-lg font-medium py-2 rounded-lg cursor-pointer mr-2 pl-6">
                      Kitchen
                    </p>
                    <div className="flex w-2/6 mt-[4px] mr-4">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6 cursor-pointer mt-2"
                        onClick={handleCountKitchenMinus}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M19.5 12h-15"
                        />
                      </svg>

                      <input
                        type="text"
                        className="w-3/6 text-center  border-none outline-none rounded-lg"
                        value={kitchen}
                        disabled
                      />
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-6 h-6 cursor-pointer mt-2"
                        onClick={handleCountKitchenPlus}
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M12 4.5v15m7.5-7.5h-15"
                        />
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-4">
                <ExtrasCard
                  ovenToggle={ovenToggle}
                  wallToggle={wallToggle}
                  windowToggle={windowToggle}
                  fridgeToggle={fridgeToggle}
                  cabinetsToggle={cabinetsToggle}
                  organizingToggle={organizingToggle}
                  dishwasherToggle={dishwasherToggle}
                  garageToggle={garageToggle}
                  microwaveToggle={microwaveToggle}
                  laundryToggle={laundryToggle}
                  blindsToggle={blindsToggle}
                  washerToggle={washerToggle}
                  handleOvenClick={handleOvenClick}
                  handleWallToggle={handleWallToggle}
                  handleWindowToggle={handleWindowToggle}
                  handleFridgeToggle={handleFridgeToggle}
                  handleCabinetsToggle={handleCabinetsToggle}
                  handleOrganizingToggle={handleOrganizingToggle}
                  handleDishWasherClick={handleDishWasherClick}
                  handleGarageClick={handleGarageClick}
                  handleMicrowaveClick={handleMicrowaveClick}
                  handleLaundryClick={handleLaundryClick}
                  handleBlindsClick={handleBlindsClick}
                  handleWasherClick={handleWasherClick}
                />
              </div>
              <div className="mr-12">
                <DateTime
                  date={date}
                  setDate={setDate}
                  time={time}
                  setTime={setTime}
                  onetime={onetime}
                  setOneTime={setOneTime}
                  weekly={weekly}
                  setWeekly={setWeekly}
                  biWeekly={biWeekly}
                  setBiWeekly={setBiWeekly}
                  monthly={monthly}
                  setMonthly={setMonthly}
                />
              </div>
              <div>
                <PersonalInfo
                  firstName={firstName}
                  setFirstName={setFirstName}
                  lastName={lastName}
                  setLastName={setLastName}
                  email={email}
                  setEmail={setEmail}
                  contact={contact}
                  setContact={setContact}
                  country={country}
                  setCountry={setCountry}
                  state={state}
                  setState={setState}
                  zipcode={zipcode}
                  setZipcode={setZipcode}
                />
              </div>
              <div className="-mt-14">
                <Discount
                  discount={discount}
                  setDiscount={setDiscount}
                  comments={comments}
                  setComments={setComments}
                />
              </div>
              <div className="mr-24 mt-12 mb-12 max-mobile:mr-4">
                <motion.button
                  className="bg-[#8cd790] border border-black mr-24 text-white mr-2 w-full text-lg py-3 rounded-lg font-medium"
                  whileTap={{ scale: 0.3 }}
                  onClick={handleBookUs}
                >
                  Continue
                </motion.button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default BookUs;
