import React from "react";
import Image from "next/image";

const MaidCards = () => {
  return (
    <>
      <div className="bg-white border border-1 border-[#8cd790]">
        <div className="w-72">
          <Image src="/icons/img9.png" alt="" width="300" height="250" />
          <p className="mt-8 pb-8 pl-5 pr-5 text-base font-normal text-center">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit
            minus, voluptatibus ut amet vitae aliquam, odio eos repellendus
            laudantium itaque unde.
          </p>
        </div>
      </div>
    </>
  );
};

export default MaidCards;
