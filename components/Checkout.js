import React, { useState } from "react";
import Image from "next/image";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { motion } from "framer-motion";
import ModalComponent from "./Modal";

const Checkout = () => {
  const [date, setDate] = useState(new Date());
  const [modalToggle, setModalToggle] = useState(false);
  const [modalLoading, setModalLoading] = useState(false);

  const [otp, setOtp] = useState("");
  const [otp1, setOtp1] = useState("");
  const [otp2, setOtp2] = useState("");
  const [otp3, setOtp3] = useState("");
  const [otp4, setOtp4] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    setModalToggle(true);
    const otp = Math.floor(1000 + Math.random() * 9000);
    const message = {
      email: "ali.baloch@launchbox.pk",
      message: `This OTP is Sent From MailBox, ${otp} Do not share it with anyone`,
    };
    const sendOTP = await fetch("/api/sendOtp", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(message),
    });
    const sendOTPResponse = await sendOTP.json();
    console.log(sendOTPResponse);
    if (sendOTPResponse.success == true) {
      setModalToggle(true);
      setOtp(otp);
    } else {
      setOtpError(true);
      setLoading(false);
    }
  };

  const onCloseModal = () => {
    setModalToggle(false);
  };
  const handleOtpCheck = (e) => {
    e.preventDefault();
    setModalLoading(true);
    setTimeout(async () => {
      const OTP = otp1 + otp2 + otp3 + otp4;
      if (OTP == parseInt(otp)) {
        console.log("matched");
      }
    }, 2000);
  };
  return (
    <>
      <div className="flex pt-44 pb-24 max-mobile:pt-20">
        <div className="w-1/2 ml-64 max-laptop1:ml-32 max-mobile:ml-12 max-mobile:w-full max-mobile:mr-10">
          <p
            className="text-5xl font-medium"
            style={{
              fontFamily: "roboto",
            }}
          >
            Checkout
          </p>
          <p
            className="text-2xl font-medium mt-4 ml-1"
            style={{
              fontFamily: "roboto",
            }}
          >
            Card Details
          </p>
          <form action="" className="ml-1">
            <div className="mt-6">
              <p className="text-lg" style={{ fontFamily: "roboto" }}>
                Name on Card
              </p>
              <input
                type="text"
                className="mt-2 border h-12 w-4/6 max-mobile:w-full max-mobile:mr-5 rounded-lg border-2 border-[#8cd790] drop-shadow-lg pl-4 pr-5 tracking-widest font-medium"
              />
            </div>
            <div className="mt-5">
              <p className="text-lg" style={{ fontFamily: "roboto" }}>
                Card Number
              </p>
              <input
                type="text"
                className="mt-2 border h-12 w-4/6 max-mobile:w-full max-mobile:mr-5 rounded-lg border-2 border-[#8cd790] drop-shadow-lg pl-4 pr-5 tracking-widest font-medium"
              />
            </div>
            <div className="mt-5 w-full">
              <div className="flex">
                <div>
                  <p className="text-lg" style={{ fontFamily: "roboto" }}>
                    Expiry Date
                  </p>
                  <DatePicker
                    selected={date}
                    onChange={(date) => setDate(date)}
                    className="mt-2 border h-12 rounded-lg w-72 max-mobile:w-44 mr-4 border-2 border-[#8cd790] drop-shadow-lg pl-4 pr-5 tracking-widest font-medium"
                  />
                </div>
                <div>
                  <p className="text-lg" style={{ fontFamily: "roboto" }}>
                    CVV
                  </p>
                  <input
                    type="text"
                    className="mt-2 border h-12 w-28 rounded-lg border-2 border-[#8cd790] drop-shadow-lg pl-4 pr-5 tracking-widest font-medium"
                  />
                </div>
              </div>
            </div>
          </form>
          <div
            className="pb-3 flex place-content-center w-4/6 mt-8 border-b-2 border-black max-mobile:w-full"
            style={{ fontFamily: "roboto" }}
          >
            <p className="text-base font-medium max-mobile:text-center">2 Rooms</p>
            <p className="text-base font-medium ml-4 max-mobile:text-center">1 TV Lounge</p>
            <p className="text-base font-medium ml-4 max-mobile:text-center">2 Washrooms</p>
          </div>
          <div style={{ fontFamily: "roboto" }} className="flex ml-10 max-mobile:ml-2">
            <p className="text-2xl mt-4 text-[#666666] font-medium">Total</p>
            <div className="flex place-content-end w-3/6 mt-4 max-mobile:w-full">
              <p
                style={{ fontFamily: "roboto" }}
                className="text-3xl font-semibold text-[#8cd790] text-end"
              >
                $100
              </p>
            </div>
          </div>
          <div className="mt-6 w-4/6 max-mobile:w-full max-mobile:text-center">
            <p
              className="uppercase text-sm text-[#666666]"
              style={{ fontFamily: "roboto" }}
            >
              I authorize Maid simpl to charge my credit card above for agreed
              upon purchases.
            </p>
            <p
              className="uppercase text-sm mt-2 text-[#666666]"
              style={{ fontFamily: "roboto" }}
            >
              i understand that my information will be saved to file for further
              transactions on my account
            </p>
            <motion.button
              className="w-full bg-[#4A4A48] text-white py-2 rounded-full mt-4 font-medium"
              whileTap={{ scale: 0.7 }}
              onClick={handleSubmit}
            >
              Make Payment
            </motion.button>
          </div>
        </div>
        <div className="w-1/2 mt-32 max-laptop1:-ml-12 max-laptop1:mr-12 max-mobile:hidden">
          <Image width={800} height={800} src="/icons/checkout.png" alt="" />
        </div>
      </div>
      <div className="flex place-content-center">
        <ModalComponent
          modaltoggle={modalToggle}
          onCloseModal={onCloseModal}
          otp1={otp1}
          setOtp1={setOtp1}
          otp2={otp2}
          setOtp2={setOtp2}
          otp3={otp3}
          setOtp3={setOtp3}
          otp4={otp4}
          setOtp4={setOtp4}
          handleOtpCheck={handleOtpCheck}
          modalLoading={modalLoading}
        />
      </div>
    </>
  );
};

export default Checkout;
