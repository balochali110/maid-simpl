import React from "react";
import Image from "next/image";
import FooterContact from "./FooterContact";

const Contact = () => {
  return (
    <>
      <div className="flex pt-32 max-mobile:pt-12">
        <div
          className="bg-[#FAFAFA] pt-12 pb-20 w-[600px] max-mobile:hidden"
          style={{
            borderBottomRightRadius: "60px",
            borderTopRightRadius: "60px",
          }}
        >
          <Image
            src="/icons/contactimg.png"
            alt="N"
            width="1400"
            height="1300"
            className="w-80 ml-44 pt-2 max-laptop1:ml-0 max-laptop1:pt-28"
          />
        </div>
        <div className="w-[1580px] ml-28 max-mobile:w-full max-mobile:ml-0">
          <div
            className="bg-[#fafafa] pt-12 pb-20 w-full"
            style={{
              borderBottomLeftRadius: "60px",
              borderTopLeftRadius: "60px",
            }}
          >
            <div className="w-full bg-[#8CD790] max-mobile:pl-4">
              <p className="pt-6 pl-36 text-4xl text-white max-mobile:pl-0 max-mobile:pr-4 max-mobile:text-3xl max-mobile:text-center">
                We would love to help
              </p>
              <p className="pt-2 pb-7 pl-36 text-2xl text-[#f8fafc] max-mobile:pl-5 max-mobile:pr-9 max-mobile:text-center">
                Reachout and we will get in touch within 24 hours
              </p>
            </div>
            <div className="ml-44 mt-8 max-laptop1:ml-32 max-laptop1:mr-12 max-mobile:ml-4 max-mobile:pl-4">
              <form action="">
                <div className="flex max-mobile:block">
                  <div>
                    <p className="text-xl font-semibold mb-2">First Name</p>
                    <input
                      type="text"
                      className="w-[400px] h-[50px] rounded-lg border pl-4 max-mobile:w-full"
                      placeholder="First Name"
                      required
                    />
                  </div>
                  <div className="ml-20 max-mobile:ml-0 max-mobile:mt-4">
                    <p className="text-xl font-semibold mb-2">Last Name</p>
                    <input
                      type="text"
                      className="w-[400px] h-[50px] rounded-lg border pl-4 max-mobile:w-full"
                      placeholder="Last Name"
                      required
                    />
                  </div>
                </div>
                <div className="flex mt-4 max-mobile:block">
                  <div>
                    <p className="text-xl font-semibold mb-2">Email</p>
                    <input
                      type="email"
                      className="w-[880px] h-[50px] rounded-lg border pl-4 max-mobile:w-full"
                      placeholder="Email"
                      required
                    />
                  </div>
                </div>
                <div className="flex mt-4 max-mobile:block">
                  <div>
                    <p className="text-xl font-semibold mb-2">Message</p>
                    <textarea
                      type="text"
                      className="w-[880px] rounded-lg border pl-4 pt-4 max-mobile:w-full"
                      placeholder="Message"
                      required
                      rows={8}
                    />
                  </div>
                </div>
                <div className="w-full mr-2">
                  <button className="bg-[#8cd790] w-[880px] rounded-lg mt-6 h-12 font-semibold text-white text-lg max-mobile:w-full">
                    Send Message
                  </button>
                </div>
              </form>
              <FooterContact />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Contact;
