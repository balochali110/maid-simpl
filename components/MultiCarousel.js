import React, { useState } from 'react';
import { motion } from 'framer-motion';

function MultiCarousel() {
  const [currentSlide, setCurrentSlide] = useState(0);

  const slides = [
    { id: 1, imageUrl: '/icons/img3.png' },
    { id: 2, imageUrl: '/icons/img2.png' },
    { id: 3, imageUrl: '/icons/img3.png' },
    { id: 4, imageUrl: '/icons/img3.png' },
    { id: 5, imageUrl: '/icons/img2.png' },
    { id: 6, imageUrl: '/icons/img3.png' },
  ];

  const slidesPerPage = 3;
  const totalSlides = slides.length;
  const totalSlidePages = Math.ceil(totalSlides / slidesPerPage);

  const slideVariants = {
    enter: {
      x: 100,
      opacity: 0,
    },
    center: {
      zIndex: 1,
      x: 0,
      opacity: 1,
    },
    exit: {
      zIndex: 0,
      x: -100,
      opacity: 0,
    },
  };

  const transition = {
    duration: 0.5,
    ease: 'easeInOut',
  };

  const goToSlide = (slideIndex) => {
    setCurrentSlide(slideIndex);
  };

  const goToPrevSlide = () => {
    setCurrentSlide((prevSlide) => (prevSlide === 0 ? totalSlidePages - 1 : prevSlide - 1));
  };

  const goToNextSlide = () => {
    setCurrentSlide((prevSlide) => (prevSlide === totalSlidePages - 1 ? 0 : prevSlide + 1));
  };

  return (
    <div className="relative">
      <div className="w-full flex overflow-hidden">
        <motion.div
          className="flex"
          style={{
            width: `${totalSlidePages * 100}%`,
            transform: `translateX(-${(currentSlide * (100 / totalSlidePages))}%`,
          }}
          transition={transition}
        >
          {slides.map((slide) => (
            <motion.div
              key={slide.id}
              className="w-full"
              variants={slideVariants}
              initial="enter"
              animate="center"
              exit="exit"
              transition={transition}
            >
              <img src={slide.imageUrl} alt={`Slide ${slide.id}`} className="w-64 h-40" />
            </motion.div>
          ))}
        </motion.div>
      </div>
      <div className="absolute bottom-0 left-0 right-0 flex justify-center">
        <button className="mx-2 p-2 bg-gray-800 text-white" onClick={goToPrevSlide}>
          Prev
        </button>
        <button className="mx-2 p-2 bg-gray-800 text-white" onClick={goToNextSlide}>
          Next
        </button>
      </div>
    </div>
  );
}

export default MultiCarousel;