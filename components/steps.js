import React from "react";
import StepsCard from "./MaidServiceCards/StepsCard";
import step1 from "../public/img/step1.png";
import step2 from "../public/img/step2.png";
import step3 from "../public/img/step3.png";
import step4 from "../public/img/step4.png";
import step5 from "../public/img/step5.png";
import step6 from "../public/img/step6.png";

const Steps = () => {
  const steps1 = [
    {
      id: 1,
      icon: step1,
      heading: "Step 1",
      desc: "Select date and time for service.",
    },
    {
      id: 2,
      icon: step2,
      heading: "Step 2",
      desc: "Select date and time for service.",
    },
    {
      id: 3,
      icon: step3,
      heading: "Step 3",
      desc: "Select date and time for service.",
    },
    {
      id: 4,
      icon: step4,
      heading: "Step 4",
      desc: "Select date and time for service.",
    },
    {
      id: 5,
      icon: step5,
      heading: "Step 5",
      desc: "Select date and time for service.",
    },
    {
      id: 6,
      icon: step6,
      heading: "Step 6",
      desc: "Select date and time for service.",
    },
  ];

  return (
    <div
      className="bg-[#EEFEF4] mt-24 pb-36"
      style={{
        fontFamily: "roboto",
      }}
    >
      <div
        className="flex place-content-center"
        style={{ fontFamily: "roboto" }}
      >
        <div className="text-4xl text-center text-black pt-28 font-semibold flex max-mobile:hidden">
          Get Started in{" "}
          <div className="w-64 ml-1 mt-[3px]">
            6-Simple Steps
            <div className="w-[240px] border-b-4 border-[#8cd790] -mt-[12px] ml-2" />
          </div>
        </div>
        <div className="min-[450px]:hidden text-2xl text-center text-black pt-28 font-semibold flex mb-8">
          Get Started in 6-Simple Steps
        </div>
      </div>
      <div className="grid grid-cols-6 max-mobile:grid-cols-1">
        {steps1.map((item) => {
          return (
            <div key={item.id}>
              <StepsCard
                id={item.id}
                heading={item.heading}
                icon={item.icon}
                desc={item.desc}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Steps;
