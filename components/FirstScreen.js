import React, { useState, useEffect, useRef } from "react";
import Hero from "../components/hero";
import Navbar from "../components/navbar";
import Footer from "../components/footer";
import Testimonials from "../components/testimonials";
import Faq from "../components/faq";
import PriceCards from "../components/MaidServiceCards/PriceCards";
import Steps from "../components/steps";
import About from "../components/About";
import Head from "next/head";
import HeroDesign from "./herodesign";
import { motion, useAnimation } from "framer-motion";

const FirstScreen = (props) => {
  const variants = {
    hidden: { opacity: 0 },
    visible: { opacity: 1 },
  };

  const transition = {
    duration: 2.5, // Set the duration here (in seconds)
  };

  const priceCards = [
    {
      id: 1,
      title: "Regular",
      semiTitle: "Starting at",
      price: 100,
      desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel, nulla. Delectus soluta, distinctio enim, provident laudantium quae dolorem corrupti alias consequuntur rerum nihil commodi obcaecati fugiat hic, laborum minus perferendis!",
    },
    {
      id: 2,
      title: "Regular",
      semiTitle: "Starting at",
      price: 100,
      desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel, nulla. Delectus soluta, distinctio enim, provident laudantium quae dolorem corrupti alias consequuntur rerum nihil commodi obcaecati fugiat hic, laborum minus perferendis!",
    },
    {
      id: 3,
      title: "Regular",
      semiTitle: "Starting at",
      price: 100,
      desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel, nulla. Delectus soluta, distinctio enim, provident laudantium quae dolorem corrupti alias consequuntur rerum nihil commodi obcaecati fugiat hic, laborum minus perferendis!",
    },
  ];

  return (
    <>
      <Head>
        <title>Maid Simpl</title>
        <link rel="icon" href="/img/favicon.png" />
      </Head>
      <Hero />
      <motion.div
        initial="hidden"
        animate="visible"
        variants={variants}
        transition={transition}
        className="max-mobile:mb-20"
      >
        <div
          className="text-center w-full text-4xl mb-4 font-bold tracking-widest relative z-10 max-mobile:text-2xl" 
          style={{
            fontFamily: "roboto",
          }}
        >
          Choose a Cleaning Plan
        </div>
        <div className="flex place-content-center -mt-6 max-mobile:hidden">
          <div className="border-b-4 border-[#8CD790] w-[274px] relative -z-10 ml-48" />
        </div>
        <div className="flex place-content-center mt-8">
          <p className="text-center text-black text-xl mt-3 w-1/3 max-mobile:text-base max-mobile:w-full max-mobile:pl-3 max-mobile:pr-3 max-laptop1:w-1/2">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt
            fuga sit veritatis perspiciatis vero ullam quia magni odio ducimus
            beat
          </p>
        </div>
        <div className="flex place-content-center mt-20 mb-12 max-mobile:mt-10 max-mobile:block max-mobile:ml-0 max-laptop1:ml-12">
          {priceCards.map((e) => {
            return (
              <div key={e.id}>
                <PriceCards />
              </div>
            );
          })}
        </div>
      </motion.div>
      <HeroDesign />
      <Steps />
      <About />
      <Testimonials />
      <Faq />
      <Footer />
    </>
  );
};

export default FirstScreen;
