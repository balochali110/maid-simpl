import React, { useState } from "react";
import "react-multi-carousel/lib/styles.css";
import TestimonialCard from "./MaidServiceCards/testimonialsCards";
import Image from "next/image";
import { motion } from "framer-motion";

const Testimonials = () => {
  const [currentSlide, setCurrentSlide] = useState(0);

  const slides = [
    {
      id: 1,
      imageUrl: "/img/img1.png",
      text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos debitis,sit dicta cumque earum voluptatum quas deleniti error consequatur incidunt.",
      name: "Henry1",
    },
    {
      id: 2,
      imageUrl: "/img/img2.png",
      text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos debitis,sit dicta cumque earum voluptatum quas deleniti error consequatur incidunt.",
      name: "Henry2",
    },
    {
      id: 3,
      imageUrl: "/img/img3.png",
      text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos debitis,sit dicta cumque earum voluptatum quas deleniti error consequatur incidunt.",
      name: "Henry3",
    },
    {
      id: 4,
      imageUrl: "/img/img2.png",
      text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos debitis,sit dicta cumque earum voluptatum quas deleniti error consequatur incidunt.",
      name: "Henry4",
    },
    {
      id: 5,
      imageUrl: "/img/img1.png",
      text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos debitis,sit dicta cumque earum voluptatum quas deleniti error consequatur incidunt.",
      name: "Henry5",
    },
    {
      id: 6,
      imageUrl: "/img/img3.png",
      text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos debitis,sit dicta cumque earum voluptatum quas deleniti error consequatur incidunt.",
      name: "Henry6",
    },
  ];

  const totalSlides = slides.length;
  const slidesPerPage = 3;

  const goToPrevSlide = () => {
    setCurrentSlide((prevSlide) =>
      prevSlide === 0 ? totalSlides - slidesPerPage : prevSlide - 1
    );
  };

  const goToNextSlide = () => {
    setCurrentSlide((prevSlide) =>
      prevSlide === totalSlides - slidesPerPage ? 0 : prevSlide + 1
    );
  };
  return (
    <>
      <div className="ml-64 max-mobile:ml-4 max-laptop1:ml-16">
        <Image
          src="/icons/icon5.png"
          width="70"
          height="70"
          className={"w-20 max-mobile:w-14"}
          alt="Hero Illustration"
        />
      </div>
      <div
        className="pb-28 bg-[#EEFEF4] mb-24 mt-[-40px] max-mobile:mb-0 max-laptop1:visible"
        style={{
          fontFamily: "roboto",
        }}
      >
        <div className="flex pt-12 max-mobile:block">
          <div className="pt-32 w-1/4 ml-64 max-mobile:ml-0 max-mobile:w-full max-mobile:pt-12 max-laptop1:ml-24 max-laptop1:w-1/3">
            <div className="text-4xl font-semibold font-sans mb-8 flex max-mobile:hidden max-laptop1:text-3xl">
              Hear From{" "}
              <div className="ml-1">
                Our Clients{" "}
                <div className="w-[170px] border-b-4 border-[#8cd790] -mt-[8px] ml-2" />
              </div>
            </div>
            <div className="text-2xl font-semibold font-sans mb-8 text-center min-[450px]:hidden">
              Hear From Our Clients
            </div>
            <p className="mr-12 text-black font-base text-lg max-mobile:text-center max-mobile:text-base max-mobile:mr-0 max-mobile:ml-0 max-mobile:pl-3 max-mobile:pr-3 max-mobile:w-96">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Aspernatur labore sunt id, libero, vitae optio temporibus dolorem,
              in at omnis repudiandae! Ea culpa maxime exercitationem non, optio
              aspernatur aut necessitatibus.
            </p>
          </div>

          <div className="flex w-4/5 ml-12 pr-12 mt-12 max-mobile:w-full max-mobile:ml-0 max-mobile:pl-12 max-mobile:pr-0 max-laptop1:w-3/5">
            <div className="w-full flex overflow-x-hidden">
              <motion.div
                className="flex"
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                transition={{ duration: 0.8 }}
              >
                {slides.map((slide, index) => {
                  const slideOffset = index - currentSlide;
                  const isVisible =
                    slideOffset >= 0 && slideOffset < slidesPerPage;
                  return (
                    <motion.div
                      key={slide.id}
                      className={`flex-shrink-0 ${
                        isVisible ? "block" : "hidden"
                      }`}
                      layout
                      animate={{ opacity: 1 }}
                      transition={{
                        layout: { duration: 1 },
                        repeat: Infinity,
                      }}
                    >
                      <TestimonialCard
                        img={slide.imageUrl}
                        text={slide.text}
                        key={slide.id}
                        name={slide.name}
                      />
                    </motion.div>
                  );
                })}
              </motion.div>
            </div>
          </div>
        </div>
        <div className="flex place-content-end mr-52 max-mobile:mr-52 max-laptop1:mr-10 mt-8 max-mobile:place-content-center">
          <div className="flex mt-6 ml-[-300px] max-mobile:hidden max-laptop1:ml-[-100px]">
            <div className="w-3 h-3 bg-[#8cd790] rounded-full mr-2"></div>
            <div className="w-3 h-3 bg-gray-300 rounded-full mr-2"></div>
            <div className="w-3 h-3 bg-gray-300 rounded-full mr-2"></div>
          </div>
          <div className="ml-[300px] max-mobile:flex max-mobile:ml-56">
            <button
              className="mx-2 p-2 bg-[#8cd790] rounded-full h-14 w-14 text-white"
              onClick={goToPrevSlide}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-8 h-8 ml-1"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M19.5 12h-15m0 0l6.75 6.75M4.5 12l6.75-6.75"
                />
              </svg>
            </button>
            <button
              className="mx-2 p-2 bg-[#8cd790] rounded-full h-14 w-14 text-white"
              onClick={goToNextSlide}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-8 h-8 ml-1"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75"
                />
              </svg>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Testimonials;
