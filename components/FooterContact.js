import React from "react";
import Image from "next/image";

const FooterContact = () => {
  return (
    <>
      <div className="flex border-t-2 mr-60 mt-6 max-mobile:block max-mobile:mr-4">
        <div className="flex w-1/2 -mt-5 max-mobile:w-full">
          <div className="w-5 mt-12 max-mobile:w-16">
            <Image src="/icons/address.png" alt="" width="800" height="800" />
          </div>
          <p className="mt-9 ml-4">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum,
            velit maxime molestiae ipsam labore cupiditate
          </p>
        </div>
        <div className="flex ml-12 -mt-5 max-mobile:ml-0">
          <div className="w-4 mt-12 max-mobile:w-5">
            <Image src="/icons/phone.png" alt="" width="800" height="800" />
          </div>
          <p className="mt-10 ml-4 max-mobile:mt-11">
            222-121-1144
          </p>
        </div>
      </div>
    </>
  );
};

export default FooterContact;
