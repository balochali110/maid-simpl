import React from "react";
import Image from "next/image";

const About = () => {
  return (
    <div className="mt-24 w-full max-mobile:mt-20 max-mobile:mb-20 max-laptop1:" style={{
      fontFamily: "roboto"
    }}>
      <div className="flex place-content-end mt-[120px] mr-28 max-mobile:hidden">
        <Image
          src="/icons/icon3.png"
          alt="N"
          width="500"
          height="500"
          className="w-36"
        />
      </div>
      <div className="-mt-[70px] max-mobile:mt-0"></div>
      <p className="text-4xl font-bold text-center font-roboto">
        <span className="border-b-4 border-[#8cd790]">About Us</span>
      </p>
      <p className="font-roboto mt-16 text-xl text-center max-mobile:text-base max-mobile:pr-4 max-mobile:pl-4">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum
        aspernatur praesentium veritatis molestias a, possimus adipisci.
        <br />
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum
        aspernatur praesentium.
      </p>
      <div className="mt-[-220px] ml-24 max-mobile:hidden max-laptop1:ml-4">
        <Image
          src="/icons/shade.png"
          alt="N"
          width="500"
          height="500"
          className="w-88"
        />
      </div>
      <div className="mt-[120px] max-mobile:hidden max-laptop1:mt-[200px]"></div>
      <div className="ml-12 max-mobile:ml-0 max-laptop1:ml-4">
        <div className="mt-[-20px] max-mobile:mt-0 max-mobile:ml-4">
          <div className="mt-[-450px] ml-24 max-mobile:mt-4 max-mobile:ml-4">
            <Image
              src="/icons/icon4.png"
              alt="N"
              width="500"
              height="500"
              className="w-80 max-mobile:w-32 max-mobile:-ml-4 max-laptop1:w-64"
            />
          </div>
          <div className="flex w-full max-mobile:block">
            <div className="mt-[-170px] ml-12 w-1/2 max-mobile:-mt-20 max-mobile:w-full max-mobile:ml-0">
              <Image
                src="/icons/img.png"
                alt="N"
                width="500"
                height="500"
                className="w-88 ml-40 max-mobile:ml-12 max-mobile:w-72 max-laptop1:w-[465px] max-laptop1:mt-5 max-mobile:mt-0"
              />
            </div>
            <div className="w-1/2 mt-[-150px] ml-[-200px] max-mobile:ml-0 max-mobile:w-full max-mobile:mt-14 max-laptop1:ml-[-30px]">
              <p className="text-xl mr-32 max-mobile:mr-2 max-mobile:text-center max-mobile:pr-2 max-mobile:pl-2 max-mobile:text-base max-laptop1:text-lg">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste
                quidem explicabo error. Perspiciatis, libero eveniet.
                Repellendus dolore ut quidem mollitia reiciendis deserunt
                assumenda libero atque nisi modi. Pariatur, quos tempore. libero
                eveniet. Repellendus dolore ut quidem mollitia reiciendis
                deserunt assumenda libero atque nisi modi. Pariatur, quos
                tempore.
                <br />
                <br className="max-laptop1:hidden" />
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste
                quidem explicabo error. Perspiciatis, libero eveniet.
                Repellendus dolore ut quidem mollitia reiciendis deserunt
                assumenda libero atque nisi modi. Pariatur, quos tempore. libero
                eveniet. Repellendus dolore ut quidem mollitia reiciendis
                deserunt assumenda libero atque nisi modi. Pariatur, quos
                tempore.
              </p>
              <div className="max-mobile:flex max-mobile:place-content-center">
              <button className="border border-2 border-black text-black px-8 py-4 max-mobile:py-2 rounded-lg text-lg font-semibold mt-8 hover:bg-[#8cd790] hover:border-[#8cd790] max-laptop1:mt-5">
                Learn More
              </button>
              </div>
            </div>
          </div>
        </div>
        <div className="flex place-content-end mt-[-280px] max-mobile:hidden">
          <Image
            src="/icons/shade.png"
            alt="N"
            width="500"
            height="500"
            className="w-88"
          />
        </div>
      </div>
    </div>
  );
};

export default About;
