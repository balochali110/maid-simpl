import React, { useState } from "react";
import ServiceProviderCard from "./MaidServiceCards/ServiceProviderCard";
import { motion } from "framer-motion";
import { useRouter } from "next/router";

const ServiceProvider = () => {
  const router = useRouter();
  let arr = [
    {
      id: 1,
      name: "Jane Doe",
    },
    {
      id: 2,
      name: "Liam Hale",
    },
    {
      id: 3,
      name: "Jane Hale",
    },
    {
      id: 4,
      name: "John Smith",
    },
    {
      id: 5,
      name: "Steven Brown",
    },
    {
      id: 6,
      name: "Steven Smith",
    },
    {
      id: 7,
      name: "Liam Brown",
    },
    {
      id: 8,
      name: "Steven hale",
    },
  ];
  const [selectedCards, setSelectedCards] = useState([]);
  const handleCardSelect = (cardId) => {
    if (selectedCards.includes(cardId)) {
      setSelectedCards(selectedCards.filter((id) => id !== cardId));
    } else if (selectedCards.length < 3) {
      setSelectedCards([...selectedCards, cardId]);
    }
  };

  const handleBookNow = () => {
    router.push("/checkout");
  };

  return (
    <>
      <div className="pt-44 mb-44 max-mobile:pt-20" style={{ fontFamily: "roboto" }}>
        <p
          className="text-center text-3xl font-semibold max-mobile:text-2xl"
          style={{
            fontFamily: "roboto",
          }}
        >
          Select Service Provider
        </p>
        <p
          className="text-center text-lg mt-4 max-mobile:text-base max-mobile:pl-4 max-mobile:pr-4"
          style={{
            fontFamily: "roboto",
          }}
        >
          Choose up to three service provider profiles; the first to accept will
          be assigned to you.
        </p>

        <div className="flex place-content-center ml-20 max-mobile:ml-6">
          <div className="grid grid-cols-2 w-4/6 gap-4 max-laptop1:w-full max-mobile:w-full max-mobile:grid-cols-1 max-mobile:gap-1">
            {arr.map((e) => {
              return (
                <div key={e.id}>
                  <ServiceProviderCard
                    id={e.id}
                    name={e.name}
                    isSelected={selectedCards.includes(e.id)}
                    onSelect={handleCardSelect}
                    card={e}
                  />
                </div>
              );
            })}
          </div>
        </div>
        <div className="flex place-content-center ml-8 max-mobile:ml-4 max-mobile:mr-4">
          <div className="mt-24 border-2 border-[#8cd790] rounded-lg font-medium pt-4 w-4/6 pb-6 max-mobile:w-full">
            <p className="uppercase text-center text-lg max-mobile:pl-5 max-mobile:pr-5 max-mobile:text-base">
              By clicking the book now button you are agreeing to our{" "}
              <span
                className="underline underline-offset-4 cursor-pointer"
                onClick={() => {
                  router.push("/terms");
                }}
              >
                {" "}
                terms of service
              </span>{" "}
              and{" "}
              <span
                className="underline underline-offset-4 cursor-pointer"
                onClick={() => {
                  router.push("/privacy");
                }}
              >
                {" "}
                privacy policy
              </span>
              .
            </p>
            <div className="ml-40 mt-4 flex max-mobile:ml-4">
              <input type="checkbox" name="" id="" className="ml-2" />
              <p className="uppercase ml-2 max-mobile:text-base max-mobile:mr-5">
                i agree to the{" "}
                <span
                  className="underline underline-offset-4 cursor-pointer"
                  onClick={() => {
                    router.push("/terms");
                  }}
                >
                  terms of service
                </span>{" "}
                and{" "}
                <span
                  className="underline underline-offset-4 cursor-pointer"
                  onClick={() => {
                    router.push("/privacy");
                  }}
                >
                  privacy policy
                </span>
              </p>
            </div>
            <div className="flex place-content-center mt-6">
              <motion.button
                className="font-medium px-[420px] py-2 bg-[#4A4A48] text-white rounded-full max-mobile:w-full max-mobile:px-0 max-mobile:ml-5 max-mobile:mr-5"
                whileTap={{ scale: 0.8 }}
                onClick={handleBookNow}
              >
                Book Now
              </motion.button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ServiceProvider;
