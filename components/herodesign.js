import React from "react";
import Image from "next/image";

const HeroDesign = () => {
  return (
    <div className="max-mobile:hidden">
      <div className="flex place-content-end mt-[-800px] mr-16">
        <Image
          src="/icons/shade.png"
          alt="N"
          width="500"
          height="500"
          className="w-88"
        />
      </div>
      <div className="mt-[800px]"></div>
      <div className="flex place-content-end mt-[-1050px]">
        <Image
          src="/icons/icon2.png"
          alt="N"
          width="300"
          height="300"
          className="w-20"
        />
      </div>
      <div className="mt-[450px]"></div>
      <div className="mt-[-400px]">
        <Image
          src="/icons/shade.png"
          alt="N"
          width="500"
          height="500"
          className="w-88"
        />
      </div>
      <div className="-mt-20"></div>
      <div className="flex place-content-start mt-[20px]">
        <Image
          src="/icons/icon1.png"
          alt="N"
          width="300"
          height="300"
          className="w-20"
        />
      </div>
      <div className="-mt-[150px]"></div>
    </div>
  );
};

export default HeroDesign;
