import Link from "next/link";
import Image from "next/image";
import { useState, useEffect } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { useRouter } from "next/router";

const Navbar = (props) => {
  const router = useRouter();

  const handleLogout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("email");
    console.log(localStorage.getItem("token"));
    window.location.reload();
  };

  const [bgColor, setBgColor] = useState("");
  useEffect(() => {
    if (
      router.pathname == "/home" ||
      router.pathname == "/contact" ||
      router.pathname == "/privacy" ||
      router.pathname == "/terms" ||
      router.pathname == "/serviceprovider" ||
      router.pathname == "/viewdetails" ||
      router.pathname == "/checkout"
    ) {
      setBgColor("#FFFFFF");
    } else if (router.pathname == "/aboutus") {
      setBgColor("#F7F7F7");
    } else if (router.pathname == "/bookus") {
      setBgColor("#EEFEF4");
    } else if (
      router.pathname == "/profile" ||
      router.pathname == "/bookinghistory"
    ) {
      setBgColor("#FAFAFA");
    }
  }, [bgColor]);

  const Login = (e) => {
    e.preventDefault();
    setTimeout(() => {
      router.push({ pathname: "/login" });
    }, 500);
  };

  const handleFaq = () => {
    if (router.pathname === "/home") {
      const element = document.getElementById("faq");
      element.scrollIntoView({ behavior: "smooth" });
    } else {
      router.push({
        pathname: "/home",
      });

      setTimeout(() => {
        const element = document.getElementById("faq");
        element.scrollIntoView({ behavior: "smooth" });
      }, 1000);
    }
  };
  const [profile, setProfile] = useState(false);
  const handleOpen = () => {
    setProfile(!profile);
  };
  const handleAbout = () => {
    router.push({
      pathname: "/aboutus",
    });
  };
  const handleContact = () => {
    router.push("/contact");
  };
  const pathname = router.pathname;

  const handleScrollOrClick = () => {
    setProfile(false);
  };

  useEffect(() => {
    // Add event listeners for scroll and click events
    window.addEventListener("scroll", handleScrollOrClick);

    return () => {
      // Clean up event listeners
      window.removeEventListener("scroll", handleScrollOrClick);
    };
  }, []);

  const handleBookings = () => {
    router.push("/bookinghistory");
  };

  const handleProfile = () => {
    router.push("/profile");
  };

  //max-mobile Responsive
  const [bar, setBar] = useState(false);
  const [showContent, setShowContent] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      setShowContent(false);
      setBar(false);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  return (
    <>
      <div
        className="relative z-30 max-mobile:hidden"
        style={{
          fontFamily: "roboto",
        }}
      >
        <div className={`fixed w-full pl-12 bg-[${bgColor}] `}>
          <motion.div
            className="relative flex items-center p-8 z-50 ml-40"
            animate={{
              y: [-250, 30, 0, 15, 0, 7, 0],
              transition: {
                duration: 3.0,
                times: [2.0, 2.2, 2.4, 2.6, 2.8, 3.0],
              },
            }}
          >
            <Link href="/">
              <span>
                <Image
                  src="/img/main-logo.png"
                  alt="N"
                  width="600"
                  height="400"
                  className={"w-56"}
                />
              </span>
            </Link>
            {/* menu  */}
            <div className={`text-center w-full flex place-content-center`}>
              <ul className="flex">
                <li className="mr-3 nav__item">
                  <Link
                    href={{
                      pathname: "/home",
                    }}
                    className={
                      pathname === "/home"
                        ? "inline-block px-4 py-2 text-lg text-[#8CD790] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790] font-semibold"
                        : "inline-block px-4 py-2 text-lg font-light text-[#4D4D4D] no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790] font-semibold"
                    }
                  >
                    HOME
                  </Link>
                </li>
                <li className="mr-3 nav__item">
                  <button
                    onClick={handleFaq}
                    className={
                      pathname === "/faq"
                        ? "inline-block px-4 py-2 text-lg text-[#4D4D4D] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790] font-semibold"
                        : "inline-block px-4 py-2 text-lg font-light text-[#4D4D4D] no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790] font-semibold"
                    }
                  >
                    FAQ
                  </button>
                </li>
                <li className="mr-3 nav__item">
                  <button
                    onClick={handleAbout}
                    className={
                      pathname === "/aboutus"
                        ? "inline-block px-4 py-2 text-lg text-[#8CD790] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790] font-semibold"
                        : "inline-block px-4 py-2 text-lg font-light text-[#4D4D4D] no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790] font-semibold"
                    }
                  >
                    ABOUT
                  </button>
                </li>
                <li className="mr-3 nav__item">
                  <Link
                    href={{
                      pathname: "/bookus",
                    }}
                    className={
                      pathname === "/bookus"
                        ? "inline-block px-4 py-2 text-lg text-[#8CD790] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790] font-semibold"
                        : "inline-block px-4 py-2 text-lg font-light text-[#4D4D4D] no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790] font-semibold"
                    }
                  >
                    BOOK US
                  </Link>
                </li>
                <li className="mr-3 nav__item">
                  <button
                    onClick={handleContact}
                    className={
                      pathname === "/contact"
                        ? "inline-block px-4 py-2 text-lg text-[#8CD790] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790] font-semibold"
                        : "inline-block px-4 py-2 text-lg font-light text-[#4D4D4D] no-underline rounded-md dark:text-gray-200 hover:text-[#8CD790] font-semibold"
                    }
                  >
                    CONTACT
                  </button>
                </li>
              </ul>
            </div>
            <div className="hidden mr-3 space-x-4 lg:flex nav__item">
              {props.loggedIn ? (
                <>
                  <div className="flex rounded-full cursor-pointer">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="#8cd790"
                      className="w-12 h-12"
                      onClick={handleOpen}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
                      />
                    </svg>
                  </div>
                </>
              ) : (
                <motion.button
                  onClick={(e) => Login(e)}
                  className="px-8 py-2 text-black font-bold rounded-md border border-1 border-black md:ml-5 hover:bg-[#8CD790] hover:text-white"
                  whileTap={{ scale: 0.7 }}
                >
                  LOGIN
                </motion.button>
              )}
            </div>
          </motion.div>
          {profile ? (
            <>
              <div className="flex place-content-end">
                <div
                  className="relative z-50 border-2 border-[#8cd790] w-48 pt-4 pb-4 pl-4 -mt-8 mr-[215px] bg-white fixed"
                  style={{
                    borderRadius: "5px",
                  }}
                >
                  <p
                    className="border-b mr-4 font-medium pb-2 hover:bg-[#8cd790] pt-2 pl-2 hover:text-white cursor-pointer rounded-lg"
                    onClick={handleBookings}
                  >
                    Bookings
                  </p>
                  <p
                    className="border-b mr-4 font-medium pb-2 pt-2 hover:bg-[#8cd790] pt-2 hover:text-white cursor-pointer pl-2 rounded-lg"
                    onClick={handleProfile}
                  >
                    My Profile
                  </p>
                  <p
                    className="border-b mr-4 font-medium pb-2 pt-2 hover:bg-[#8cd790] pt-2 hover:text-white cursor-pointer pl-2 rounded-lg"
                    onClick={handleLogout}
                  >
                    Logout
                  </p>
                </div>
              </div>
              <div className="-mt-[127px] bg-white" />
            </>
          ) : (
            ""
          )}
        </div>
      </div>
      {/* max-mobile Navbar */}

      <AnimatePresence>
        {showContent && (
          <motion.div
            className="fixed top-0 left-0 w-1/2 h-full z-40"
            initial={{ x: "-100%" }}
            animate={{ x: "0%" }}
            exit={{ x: "-100%" }}
            transition={{ duration: 0.3 }}
          >
            <div className="bg-white w-full h-full">
              <div className="flex place-content-center pt-20">
                <div>
                  <span className="flex place-content-start ml-2 mt-4">
                    <Image
                      src="/img/main-logo.png"
                      width={200}
                      height={200}
                      alt=""
                      className="w-28 px-2 py-2"
                    />
                  </span>
                  <div className="mt-12 text-left ml-4 text-sm">
                    <Link
                      href={{
                        pathname: "/home",
                      }}
                    >
                      <p
                        className={
                          pathname === "/home"
                            ? "uppercase font-semibold text-[#8cd790] mb-5 cursor-pointer"
                            : "uppercase font-semibold text-black/[0.6] mb-5 cursor-pointer hover:text-[#8cd790]"
                        }
                      >
                        Home
                      </p>
                    </Link>

                    <p
                      onClick={handleFaq}
                      className={
                        pathname === "/faq"
                          ? "uppercase font-semibold text-[#8cd790] mb-5 cursor-pointer"
                          : "uppercase font-semibold text-black/[0.6] mb-5 cursor-pointer hover:text-[#8cd790]"
                      }
                    >
                      FAQ
                    </p>
                    <Link
                      href={{
                        pathname: "/aboutus",
                      }}
                    >
                      <p
                        className={
                          pathname === "/aboutus"
                            ? "uppercase font-semibold text-[#8cd790] mb-5 cursor-pointer"
                            : "uppercase font-semibold text-black/[0.6] mb-5 cursor-pointer hover:text-[#8cd790]"
                        }
                      >
                        About
                      </p>
                    </Link>
                    <Link
                      href={{
                        pathname: "/bookus",
                      }}
                    >
                      <p
                        className={
                          pathname === "/bookus"
                            ? "uppercase font-semibold text-[#8cd790] mb-5 cursor-pointer"
                            : "uppercase font-semibold text-black/[0.6] mb-5 cursor-pointer hover:text-[#8cd790]"
                        }
                      >
                        Book Us
                      </p>
                    </Link>
                    <Link
                      href={{
                        pathname: "/contact",
                      }}
                    >
                      <p
                        className={
                          pathname === "/contact"
                            ? "uppercase font-semibold text-[#8cd790] mb-5 cursor-pointer"
                            : "uppercase font-semibold text-black/[0.6] mb-5 cursor-pointer hover:text-[#8cd790]"
                        }
                      >
                        Contact
                      </p>
                    </Link>
                    <Link
                      href={{
                        pathname: "/bookinghistory",
                      }}
                    >
                      <p
                        className={
                          pathname === "/bookinghistory"
                            ? "uppercase font-semibold text-[#8cd790] mb-5 cursor-pointer"
                            : "uppercase font-semibold text-black/[0.6] mb-5 cursor-pointer hover:text-[#8cd790]"
                        }
                      >
                        Booking History
                      </p>
                    </Link>
                    <Link
                      href={{
                        pathname: "/profile",
                      }}
                    >
                      <p
                        className={
                          pathname === "/profile"
                            ? "uppercase font-semibold text-[#8cd790] mb-5 cursor-pointer"
                            : "uppercase font-semibold text-black/[0.6] mb-5 cursor-pointer hover:text-[#8cd790]"
                        }
                      >
                        profile
                      </p>
                    </Link>
                    <p className="uppercase font-semibold mb-5 cursor-pointer text-black/[0.6] hover:text-[#8cd790]">
                      Logout
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </motion.div>
        )}
      </AnimatePresence>
      <div className="min-[450px]:hidden w-full">
        <div className="flex">
          <div className="mt-10 w-1/3 z-50">
            <span className="flex place-content-center w-full pr-6">
              <motion.svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-8 h-8"
                onClick={() => {
                  setBar(!bar);
                  setShowContent(!showContent);
                }}
                initial={{ opacity: 0, rotate: 0 }}
                animate={{
                  opacity: 1,
                  rotate: bar ? 90 : 0,
                  transition: { duration: 0.3 },
                }}
              >
                {!bar ? (
                  <motion.path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                  />
                ) : (
                  <motion.path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M6 18L18 6M6 6l12 12"
                  />
                )}
              </motion.svg>
            </span>
          </div>
          <div className="ml-4 mt-5 w-2/3">
            <Image
              src="/img/main-logo2.png"
              width={2000}
              height={2000}
              alt=""
              className={showContent ? "w-40 hidden" : "w-40"}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
