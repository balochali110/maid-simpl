import React from "react";
import Image from "next/image";

const Terms = () => {
  return (
    <>
      <div className="">
        <p className="pt-48 text-center font-bold text-5xl tracking-wide">
          Terms & Conditions
        </p>
        <br />
        <div className="flex place-content-end mr-48">
          <Image src="/icons/icon8.png" width={180} height={180} alt="" />
        </div>
        <div className="bg-[#F7F4F4] ml-64 mr-64 mt-8 rounded-lg mt-[-110px] relative z-10">
          <p className="pt-12 pl-16 pr-16 pb-8 text-lg font-medium">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit.
            Reprehenderit, est animi labore eius voluptatibus unde voluptate
            ratione veniam sint sequi aut consequatur natus, incidunt, esse
            laudantium quod temporibus dolorum porro! Lorem ipsum, dolor sit
            amet consectetur adipisicing elit. Ipsa dicta cumque porro,
            asperiores animi voluptatum quibusdam sequi facilis blanditiis
            <br />
            <br />
            dolorum. Dolores a id dignissimos suscipit, veritatis ab porro
            laboriosam aliquam omnis maxime. Lorem ipsum dolor sit amet
            consectetur, adipisicing elit. Provident, optio quo natus ea
            excepturi eius necessitatibus sit, quasi explicabo, rem id! Sapiente
            voluptatum sint illo in, est hic provident consequatur!
            <br />
            <br />
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint
            consectetur eius magni animi quasi, laboriosam a modi rem
            praesentium unde minus explicabo eum repudiandae voluptatibus!
            Voluptates vitae ullam quas autem.
            <br />
            <br />
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat
            soluta sapiente accusantium exercitationem impedit maxime qui natus
            atque consequatur libero nihil facere doloribus ipsum, nobis a ab
            odio itaque. Velit! Lorem ipsum dolor sit amet consectetur
            voluptatum labore nisi possimus, vel perferendis.
            <br />
            <br />
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint
            consectetur eius magni animi quasi, laboriosam a modi rem
            praesentium unde minus explicabo eum repudiandae voluptatibus!
            Voluptates vitae ullam quas autem.
            <br />
            <br />
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat
            soluta sapiente accusantium exercitationem impedit maxime qui natus
            atque consequatur libero nihil facere doloribus ipsum, nobis a ab
            odio itaque. Velit! Lorem ipsum dolor sit amet consectetur
            adipisicing elit. Iste architecto beatae dolor et delectus nam nisi
            eveniet? Provident, molestias quibusdam debitis iste facilis rerum
            voluptatum labore nisi possimus, vel perferendis.
          </p>
        </div>
        <div className="flex place-content-start ml-44 -mt-24">
          <Image src="/icons/icon9.png" width={140} height={140} alt="" />
        </div>
      </div>
      <br />
      <br />
    </>
  );
};

export default Terms;
