import React from "react";
import Image from "next/image";
import AboutUs2 from "./AboutUs-2";

const AboutUs = () => {
  return (
    <div
      className="pt-44 pb-12 bg-[#F7F7F7] max-mobile:pt-20"
      style={{
        fontFamily: "roboto",
      }}
    >
      <p className="font-bold text-center text-3xl">About Us</p>
      <div className="flex place-content-center mt-5">
        <p className="w-3/6 text-lg text-center max-mobile:w-full max-mobile:pl-6 max-mobile:pr-6 max-laptop1:w-4/6">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid,
          recusandae quaerat, nesciunt tenetur magnam nostrum necessitatibus
          molestias fuga ad voluptatibus libero deleniti doloribus praesentium
          beatae facilis repellat accusamus quas natus. lorem
          <br />
          <br /> Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto
          minima libero quo magnam temporibus dolores molestiae aspernatur
          deleniti, ex dolore, officiis quasi praesentium consequatur maxime
          possimus mollitia non. Fuga, praesentium! <br />
          <br />
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam,
          natus. Cupiditate, doloremque nulla beatae ea facere eveniet
          blanditiis tempora, facilis quasi iusto earum provident nihil iure
          aliquid repellat, impedit accusantium.
        </p>
      </div>

      {/* Image Section */}
      <div className="flex place-content-end mr-96 mt-[-20px] max-laptop1:mr-32 max-mobile:hidden">
        <Image src="/icons/icon8.png" width="180" height="180" alt="" />
      </div>
      <div className="flex place-content-start ml-36 mt-[-220px] max-mobile:hidden">
        <Image src="/icons/shade2.png" width="380" height="380" alt="" />
      </div>
      <div className="flex place-content-center mt-[-295px] relative max-mobile:mt-4 max-mobile:block">
        <div className="w-96 max-mobile:w-72 max-mobile:ml-12 max-mobile:hidden">
          <Image
            src="/icons/img5.png"
            width="332"
            height="400"
            alt=""
            className="max-mobile:rounded-lg"
          />
        </div>
        <div className="w-96 -ml-8 max-mobile:hidden">
          <Image src="/icons/img6.png" width="265" height="350" alt="" />
        </div>
        <div className="w-96 -ml-24 max-mobile:hidden">
          <Image src="/icons/img7.png" width="550" height="450" alt="" />
          <Image
            src="/icons/img8.png"
            width="550"
            height="450"
            alt=""
            className="mt-4"
          />
        </div>
      </div>
      <div className="flex place-content-center ml-[-360px] mt-[-70px] max-mobile:hidden">
        <Image src="/icons/icon9.png" width="140" height="140" alt="" />
      </div>
      <div className="flex place-content-end mr-44 mt-[-250px] max-mobile:hidden">
        <Image src="/icons/shade2.png" width="380" height="380" alt="" />
      </div>
      <div className="relative">
        <AboutUs2 />
      </div>
    </div>
  );
};

export default AboutUs;
