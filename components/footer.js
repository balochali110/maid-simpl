import Link from "next/link";
import Image from "next/image";
import React from "react";
import { useRouter } from "next/router";

export default function Footer() {
  const router = useRouter();
  const token = router.query.token;

  const handleLogin = () => {
    router.push("/login");
  };

  const handleContact = () => {
    router.push("/contact");
  };

  const handleAbout = () => {
    router.push("/aboutus");
  };

  const handleBook = () => {
    router.push("/bookus");
  };

  const handlePrivacy = () => {
    router.push("/privacy");
  };

  const handleTerms = () => {
    router.push("/terms");
  };
  return (
    <>
      <div className="max-mobile:mb-12">
        <div className="w-full bg-[#8cd790] h-6 mb-3 rounded-t-full" />
        <div
          className="flex border-b-2 border-black mb-12 ml-40 mr-40 pb-4 max-mobile:ml-0 max-mobile:mr-0 max-mobile:block"
          style={{
            fontFamily: "roboto",
          }}
        >
          <div className="mt-6 mb-4 ml-8 w-1/3 max-mobile:w-full max-mobile:ml-0 max-mobile:pl-4">
            <Image
              src="/img/main-logo.png"
              width="800"
              height="800"
              alt=""
              className="w-72 max-mobile:w-72 max-mobile:ml-8 max-laptop1:w-64"
            />
            <p className="font-medium mt-4 text-lg max-mobile:text-center max-mobile:pr-4 max-laptop1:pr-2 max-laptop1:text-base">
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Amet
              ipsum quaerat facilis repudiandae.
            </p>
            <div className="mt-4 max-mobile:hidden">
              <Image src="/icons/icon3.png" width="100" height="100" alt="" />
            </div>
          </div>
          <div className="ml-12 mt-12 max-mobile:ml-0 max-laptop1:w-2/3 max-mobile:w-full">
            <div className="grid grid-cols-6 gap-0 max-laptop1:grid-cols-3 max-laptop1:gap-4 max-laptop1:text-center max-mobile:grid-cols-2 max-mobile:text-center">
              <p
                className="text-lg text-[#8cd790] font-medium cursor-pointer border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
                onClick={handleLogin}
              >
                Login
              </p>
              <p
                className="text-lg text-[#8cd790] font-medium cursor-pointer border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
                onClick={handleContact}
              >
                Contact Us
              </p>
              <p
                className="text-lg text-[#8cd790] font-medium cursor-pointer border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
                onClick={handleAbout}
              >
                About
              </p>
              <p
                className="text-lg text-[#8cd790] font-medium cursor-pointer border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
                onClick={handleBook}
              >
                Book Now
              </p>
              <p
                className="text-lg text-[#8cd790] font-medium cursor-pointer border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
                onClick={handlePrivacy}
              >
                Privacy Policy
              </p>
              <p
                className="text-lg text-[#8cd790] font-medium cursor-pointer border-b-2 border-transparent hover:border-b-2 hover:border-[#8cd790]"
                onClick={handleTerms}
              >
                Terms & Conditions
              </p>
            </div>
            <div className="mt-24 ml-12 max-mobile:mt-14">
              <div className="flex">
                <span>
                  <Image
                    src="/icons/address.png"
                    width="15"
                    height="18"
                    alt=""
                    className="h-6 w-4 mt-2"
                  />
                </span>
                <p className="text-base ml-4">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  <br /> Debitis quis pariatur soluta, cum quos
                </p>
              </div>
              <div className="flex mt-4">
                <Image
                  src="/icons/phone.png"
                  width="15"
                  height="18"
                  alt=""
                  className="h-4 mt-1"
                />
                <p className="text-base ml-4">202-555-4458</p>
              </div>
            </div>
          </div>
        </div>
        <div className="w-full flex max-mobile:block">
          <div className="ml-44 mb-12 tracking-widest font-medium w-1/2 max-mobile:w-full max-mobile:ml-0 max-mobile:mb-0">
            <p className="max-mobile:hidden">
              2021 MAIDSIMPL ALL RIGHT RESERVED
            </p>
          </div>
          <div className="flex w-1/2 place-content-end mr-44 max-mobile:w-full max-mobile:place-content-center max-mobile:mr-0">
            <Image
              width="25"
              height="25"
              src="/icons/linkedin.png"
              alt=""
              className="ml-3 h-7 w-7"
            />
            <Image
              width="25"
              height="25"
              src="/icons/facebook.png"
              alt=""
              className="ml-3 h-7 w-7"
            />
            <Image
              width="25"
              height="25"
              src="/icons/instagram.png"
              alt=""
              className="ml-3 h-7 w-7"
            />
          </div>
          <p className="min-[450px]:hidden text-center mt-4 font-semibold font-roboto">
          2021 MAIDSIMPL ALL RIGHT RESERVED
          </p>
        </div>
      </div>
    </>
  );
}
