import {Schema, model, models} from "mongoose";

const SignupSchema = new Schema ({
    name: {
        type: String,
    },
    preferredname: {
        type: String
    },
    email : {
        type: String, 
    },
    password: {
        type: String,
    }
})

const Signup = models.Signup || model("Signup", SignupSchema)
export default Signup;