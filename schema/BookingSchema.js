import { Schema, model, models } from "mongoose";

const BookingS = new Schema({
  id: {
    type: Number
  },
  servicetype: {
    type: String,
  },
  home: {
    type: Array,
  },
  extras: {
    type: Array,
  },
  date: {
    type: String,
  },
  time: {
    type: String,
  },
  howoften: {
    type: String,
  },
  firstname: {
    type: String,
  },
  lastname: {
    type: String,
  },
  email: {
    type: String,
  },
  contact: {
    type: String,
  },
  country: {
    type: String,
  },
  state: {
    type: String,
  },
  zipcode: {
    type: Number,
  },
  comments: {
    type: String,
  },
  discout: {
    type: String,
  }
});

const Booking = models.BookingS || model("BookingS", BookingS);
export default Booking;
