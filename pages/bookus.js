import React, { useEffect, useState } from "react";
import Head from "next/head";
import BookUs from "../components/BookUs";
import Navbar from "../components/navbar";
import Footer from "../components/footer";
import { verifyToken } from "../lib/auth";

const bookus = () => {
  const [loggedIn, setLoggedIn] = useState(false);
  const [email, setEmail] = useState("");
  useEffect(() => {
    const token = localStorage.getItem("token");
    const email = localStorage.getItem("email");
    setEmail(email);
    if (token) {
      verifyToken(token)
        .then((data) => {
          if (data) {
            setLoggedIn(true);
          } else {
            setLoggedIn(false);
          }
        })
        .catch((error) => {
          console.error("Token verification error:", error);
          setLoggedIn(false);
        });
    } else {
      setLoggedIn(false);
    }
  }, []);
  return (
    <>
      <Head>
        <title>Maid Simpl</title>
        <meta
          name="description"
          content="Nextly is a free landing page template built with next.js & Tailwind CSS"
        />
        <link rel="icon" href="/img/favicon.png" />
      </Head>
      <Navbar loggedIn={loggedIn} email={email} />
      <BookUs />
      <Footer />
    </>
  );
};

export default bookus;
