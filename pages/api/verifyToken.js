import jwt from "jsonwebtoken";

export default function handler(req, res) {
  if (req.method === "POST") {
    const { token } = req.body;

    try {
      // Verify the token
      const decodedToken = jwt.verify(token, process.env.JWT_SECRET);

      // Token is valid
      res.status(200).json(decodedToken);
    } catch (error) {
      // Token verification failed
      res.status(401).json({ message: "Token verification failed" });
    }
  } else {
    res.status(405).json({ message: "Method not allowed" });
  }
}
