import BookingPost from "../../controllers/BookingPost.js";
import connection from "../../lib/connection.js";
import GetBooking from "../../controllers/GetBooking.js"

const booking = async (req, res) => {
  await connection();
  const method = req.method;
  switch (method) {
    case "POST":
      let result = await BookingPost(req, res);
      break;
    case "GET":
      let getBooking = await GetBooking(req, res)
      break;
    }
};

export default booking;
