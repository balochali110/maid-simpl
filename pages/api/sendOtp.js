import nodemailer from "nodemailer";

export default async function OTP(req, res) {
  const { email, message } = req.body;

  let transporter = nodemailer.createTransport({
    port: 465,
    host: "smtp.gmail.com",
    secure: true,
    auth: {
      user: process.env.USER, // generated ethereal user
      pass: process.env.PASS, // generated ethereal password
    },
  });

  const mailOptions = {
    from: "alibaloch.launchbox@gmail.com", // Sender address
    to: email, // List of recipients
    subject: "One Time Verification From Maid Simpl", // Subject line
    text: message, // Plain text body
  };

  transporter.sendMail(mailOptions, function (err, info) {
    if (err) {
      res.status(422).json({ success: false, Error: err });
    } else {
      res.status(200).json({ success: true, info: info });
    }
  });
}
