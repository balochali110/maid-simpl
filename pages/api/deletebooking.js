import DeleteBooking from "../../controllers/DeleteBooking.js";
import connection from "../../lib/connection.js";

const booking = async (req, res) => {
  await connection();
  const method = req.method;
  switch (method) {
    case "POST":
      let result = await DeleteBooking(req, res);
      break;
    }
};

export default booking;
