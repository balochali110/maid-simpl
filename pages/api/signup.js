import getUsers from "../../controllers/getUsers.js";
import SignUp from "../../controllers/signup.js";
import connection from "../../lib/connection.js";

const signup = async (req, res) => {
  await connection();
  const method = req.method;
  switch (method) {
    case "POST":
      let result = await SignUp(req, res);
      break;
    case "GET":
      let getResult = await getUsers(req, res);
      break;
  }
};

export default signup;
