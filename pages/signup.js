import React from "react";
import Head from "next/head";
import Navbar from "../components/navbar";
import Login from "../components/account/login";
import { motion } from "framer-motion";
import Signup from "../components/account/signup";

const signup = () => {
  const variants = {
    hidden: { opacity: 0, x: -200, y: 0 },
    enter: { opacity: 1, x: 0, y: 0 },
    exit: { opacity: 0, x: 0, y: -100 },
  };
  return (
    <>
      <Head>
        <title>Maid Simpl</title>
        <meta
          name="description"
          content="Nextly is a free landing page template built with next.js & Tailwind CSS"
        />
        <link rel="icon" href="/img/favicon.png" />
      </Head>
      <Signup />
    </>
  );
};

export default signup;
