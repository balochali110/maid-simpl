import React, { useEffect, useState } from "react";
import FirstScreen from "../components/FirstScreen";
import Head from "next/head";
import { verifyToken } from "../lib/auth";
import Navbar from "../components/navbar";

const home = () => {
  const [loggedIn, setLoggedIn] = useState(false);
  const [email, setEmail] = useState("");
  useEffect(() => {
    const token = localStorage.getItem("token");
    const email = localStorage.getItem("email");
    setEmail(email);
    if (token) {
      verifyToken(token)
        .then((data) => {
          if (data) {
            setLoggedIn(true);
          } else {
            setLoggedIn(false);
          }
        })
        .catch((error) => {
          console.error("Token verification error:", error);
          setLoggedIn(false);
        });
    } else {
      setLoggedIn(false);
    }
  }, []);
  return (
    <div>
      <Head>
        <title>Maid Simpl</title>
        <meta
          name="description"
          content="Nextly is a free landing page template built with next.js & Tailwind CSS"
        />
        <link rel="icon" href="/img/favicon.png" />
      </Head>
      <Navbar loggedIn={loggedIn} />
      <div className="">
        <FirstScreen loggedIn={loggedIn} email={email} />
      </div>
    </div>
  );
};

export default home;
