import BookingS from "../schema/BookingSchema.js";

export default async function BookingPost(req, res) {
  try {
    const {
      id,
      servicetype,
      home,
      extras,
      date,
      time,
      howoften,
      firstname,
      lastname,
      email,
      contact,
      country,
      state,
      zipcode,
      comments,
      discount,
    } = req.body;

    const booking = new BookingS({
      id: id,
      servicetype: servicetype,
      home: home,
      extras: extras,
      date: date,
      time: time,
      howoften: howoften,
      firstname: firstname,
      lastname: lastname,
      email: email,
      contact: contact,
      country: country,
      state: state,
      zipcode: zipcode,
      comments: comments,
      discount: discount,
    });

    const saveBooking = await booking.save();
    res.status(200).json({ success: true, result: saveBooking });
  } catch (err) {
    res.status(200).json({ success: false, error: err });
  }
}
