import bcrypt from "bcryptjs";
import Signup from "../schema/SignUpSchema";
import Joi from "@hapi/joi";

const registerSchema = Joi.object({
  name: Joi.string().min(3).required(),
  email: Joi.string().min(3).required(),
  preferredName: Joi.string().min(3),
  password: Joi.string().min(8).required(),
});

export default async function SignUp(req, res) {
  const { name, preferredName, email, password } = req.body;
  const emailExist = await Signup.findOne({ email: email });

  if (emailExist) {
    res.status(400).send({ Error: "Email Already Exist!" });
    return;
  }

  const salt = await bcrypt.genSalt(10);
  const hashedPass = await bcrypt.hash(password, salt);

  const user = new Signup({
    name: name,
    email: email,
    preferredname: preferredName,
    password: hashedPass,
  });


  try {
    const { error } = await registerSchema.validateAsync(req.body);

    if (error) {
      res.status(400).send({ Error: error.details[0].message });
    } else {
      const saveUser = await user.save();
      res.status(200).json({ result: "User Created Successfully" });
    }
  } catch (err) {
    res.status(500).send({ Error: err });
  }
}
