import Signup from "../schema/SignUpSchema";

export default async function DeleteUsers(req, res) {
  const { email } = req.body;
  Signup.deleteMany({ email: email })
    .then((response) => {
      res.status(200).json({
        success: true,
        result: response,
      });
    })
    .catch((err) => {
      res.status(422).json({
        error: err,
      });
    });
}
